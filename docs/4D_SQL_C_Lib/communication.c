#include "utils.h"
#include "communication.h"
#include "fourd_int.h"
#ifdef WIN32
#define EINPROGRESS WSAEWOULDBLOCK
#else
#include <fcntl.h>
#endif

/* The function receives data from a connected 4D server socket
 * or a bound connectionless socket.
 * Uses recv() */
long frecv(SOCKET s,unsigned char *buf,int len,int flag)
{
	int rec = 0;
	long iResult = 0;

	do{
		iResult = recv(s,buf+rec,(size_t)len-rec,flag);
		if(iResult == SOCKET_ERROR){
			if (VERBOSE == 1)
				Printferr("Error in frecv() -> recv() failed with error: %d\n",WSAGetLastError());

			return SOCKET_ERROR;
		}else {
			rec+=iResult;
		}

	}while(rec<len);

	return rec;
}

/* Opens a socket connection to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_connect(FOURD *cnx,const char *host,unsigned int port)
{
	struct addrinfo *result = NULL, *ptr = NULL, hints;
	int iResult = 0;
	char sport[50];
	sprintf_s(sport,50,"%d",port);

	// Initialize hints
	ZeroMemory( &hints, sizeof(hints) );
	hints.ai_family = AF_INET; // IPv4
	hints.ai_socktype = SOCK_STREAM; // TCP
	hints.ai_protocol = IPPROTO_TCP; // TCP

	// Resolve the 4D server address and port
	iResult = getaddrinfo(host, sport, &hints, &result);
	if ( iResult != 0 ) {
		if (VERBOSE == 1)
			Printferr("Error in socket_connect() -> getaddrinfo() failed with error: %d, %s\n",
					  iResult,gai_strerror(iResult));

		cnx->status = FOURD_ERROR;
		cnx->error_code = iResult;
		sprintf_s(cnx->error_string,ERROR_STRING_LENGTH,gai_strerror(iResult));
		freeaddrinfo(result);
		return FOURD_ERROR;
	}

	// Try to create a socket and connect getaddrinfo can
	// return multiple structures try all
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next){
		iResult = 0;

		// Create a socket to connect to the 4D server
		cnx->socket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (cnx->socket != INVALID_SOCKET){

			iResult = connect( cnx->socket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR)
			{
				closesocket(cnx->socket);
			} else{
				break; /* Connection OK */
			}

		}

	}

	// Invalid socket to 4D server
	if (cnx->socket == INVALID_SOCKET) {
		if (VERBOSE == 1)
			Printferr("Error in socket_connect() -> socket() failed with error: %ld\n", WSAGetLastError());

		cnx->status = FOURD_ERROR;
		cnx->error_code = WSAGetLastError();
		sprintf_s(cnx->error_string,ERROR_STRING_LENGTH,"Unable to create a socket to the 4D server");
		freeaddrinfo(result);
		return FOURD_ERROR;
	}

	// Couldn't connect to 4D server
	if (iResult == SOCKET_ERROR) {
		if (VERBOSE == 1)
			Printferr("Error in socket_connect() -> connect() failed with error: %d\n", WSAGetLastError());

		cnx->status = FOURD_ERROR;
		cnx->error_code = WSAGetLastError();
		sprintf_s(cnx->error_string,ERROR_STRING_LENGTH,"Unable to connect to the 4D server");
		freeaddrinfo(result);
		closesocket(cnx->socket);
		cnx->socket = INVALID_SOCKET;
		return FOURD_ERROR;
	}

	freeaddrinfo(result);
	return FOURD_OK;
}

/* Closes a socket connection to 4D server */
void socket_disconnect(FOURD *cnx)
{
#ifdef WIN32
	int iResult=0;
	iResult = shutdown(cnx->socket, SD_SEND); /* SD_SEND => Shutdown send operations */
	if (iResult == SOCKET_ERROR) {
		if (VERBOSE == 1)
			Printferr("Error in socket_disconnect() failed with error: %d\n", WSAGetLastError());

		cnx->status = FOURD_ERROR;
		cnx->error_code = WSAGetLastError();
		sprintf_s(cnx->error_string,ERROR_STRING_LENGTH,"Unable to disconnect socket");
	}
#endif
	closesocket(cnx->socket);
	cnx->connected=0;
}

/* Sends a msg to the 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_send(FOURD *cnx,const char*msg)
{
	ssize_t iResult=0;

	if(cnx->connected == 0)
	{
		if (VERBOSE == 1)
			Printferr("Socket not connected\n");

		return FOURD_ERROR;
	}

	if (DEBUG == 1)
		printf("Message sent:\n%s",msg);

	iResult = send( cnx->socket, msg, (int)strlen(msg), 0 );
	if (iResult == SOCKET_ERROR)
	{
		if (VERBOSE == 1)
			Printferr("Error in socket_send() -> send() failed with error: %ld\n", WSAGetLastError());

		cnx->status = FOURD_ERROR;
		cnx->error_code = WSAGetLastError();
		sprintf_s(cnx->error_string,ERROR_STRING_LENGTH,"Error sending data to 4D server");
		socket_disconnect(cnx);
		return FOURD_ERROR;
	}

	return FOURD_OK;
}

/* Sends a msg to the 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_send_data(FOURD *cnx,const char*msg,size_t len)
{
	long iResult;

	if(cnx->connected == 0)
	{
		if (VERBOSE == 1)
			Printferr("Socket not connected\n");

		return FOURD_ERROR;
	}

	if (DEBUG == 1)
	{
		Printf("Bytes sent: %d bytes\n",len);
		PrintData(msg,len);
		Printf("\n");
	}

	// Send an initial buffer
	iResult = send( cnx->socket, msg, len, 0 );
	if (iResult == SOCKET_ERROR) {
		if (VERBOSE == 1)
			Printferr("Error in socket_send_data() -> send() failed with error: %ld\n", WSAGetLastError());

		cnx->status = FOURD_ERROR;
		cnx->error_code = WSAGetLastError();
		sprintf_s(cnx->error_string,ERROR_STRING_LENGTH,"Error sending data to 4D server");
		socket_disconnect(cnx);
		return FOURD_ERROR;
	}

	return FOURD_OK;
}

/* Receives a msg header from the 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_receiv_header(FOURD *cnx,FOURD_RESULT *state)
{
	long iResult = 0;
	int offset = 0;
	unsigned int len =0 ;
	int crlf = 0;
	unsigned int grow_size = HEADER_GROW_SIZE;
	unsigned int new_size = grow_size;

	if(cnx->connected == 0)
	{
		if (VERBOSE == 1)
			Printferr("Socket not connected\n");

		return FOURD_ERROR;
	}

	// Allocate some space to start with
	state->header=calloc(sizeof(char),new_size);

	// Read the HEADER only
	do
	{
		offset+=iResult;
		iResult = recv(cnx->socket,state->header+offset,1, 0);
        if(iResult == SOCKET_ERROR){
            if (VERBOSE == 1)
                Printferr("Error in recv() failed with error: %d\n",WSAGetLastError());

            state->status = FOURD_ERROR;
            state->error_code = WSAGetLastError();
            sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
            return FOURD_ERROR;
        }
		len+=iResult;
		if(len>new_size-5){
			// Header storage nearly full allocate more
			new_size=new_size+sizeof(char)*grow_size;
			state->header=realloc(state->header,new_size);
		}
		if(len>3)
		{
			if(state->header[offset-3]=='\r' && state->header[offset-2]=='\n'
			   && state->header[offset-1]=='\r' && state->header[offset]=='\n')
				crlf=1;
		}

	}while(iResult>0 && !crlf);

	if(!crlf){
		if (VERBOSE == 1)
			Printferr("Error in socket_receiv_header() -> failed with error: Header-End not found\n");

		state->status = FOURD_ERROR;
		state->error_code = FOURD_ERROR;
		sprintf_s(state->error_string,ERROR_STRING_LENGTH,"Header-End not found");
		return FOURD_ERROR;
	}

	state->header[len]= 0;
	state->header_size = len;

	if (DEBUG == 1)
		printf("Received header:\n%s",state->header);

	return FOURD_OK;
}

/* Receives data from the 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_receiv_data(FOURD *cnx,FOURD_RESULT *state)
{
	long iResult = 0;
	int len = 0;
	unsigned int nbCol = state->row_type.nbColumn;
	unsigned int nbRow = state->row_count_sent;
	unsigned int r,c = 0;
	FOURD_TYPE *colType = NULL;
	FOURD_ELEMENT *pElmt = NULL;
	unsigned char status_code = 0;
	int elmts_offset = 0;
	int row_id = 0;

	if(cnx->connected == 0)
	{
		if (VERBOSE == 1)
			Printferr("Socket not connected\n");

		return FOURD_ERROR;
	}

	/*
	if (DEBUG == 1)
		Printf("Start debugging the socket_receiv_data()\n");
	 */

	colType=calloc(nbCol,sizeof(FOURD_TYPE));

	// Buffer size column type
	for(c=0;c<state->row_type.nbColumn;c++)
		colType[c]=state->row_type.Column[c].type;

	/*
	if (DEBUG == 1)
		Printf("nbCol*nbRow: %d\n",nbCol*nbRow);
	 */

	// Allocate nbElmt in state->elmt
	state->elmt=calloc(nbCol*nbRow,sizeof(FOURD_ELEMENT));

	/*
	if (DEBUG == 1){
		Printf("state->row_count: %d\t\tstate->row_count_sent:%d\n",state->row_count,state->row_count_sent);
		Printf("NbRow to read: %d\n",nbRow);
	}*/

	/* Read all rows */
	for(r=0;r<nbRow;r++){

		// Treat the status_code and row_id
		// rowId is sent only if row updateability
		if(state->updateability){
			row_id=0;
			status_code=0;

			iResult = frecv(cnx->socket,&status_code,sizeof(status_code), 0);
			if (iResult == SOCKET_ERROR){
				free(colType);
				state->status = FOURD_ERROR;
				state->error_code = WSAGetLastError();
				sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
				return FOURD_ERROR;
			}

			/*
			if (DEBUG == 1)
				Printf("status_code for row:0x%X\n",status_code);
			 */

			len+=iResult;

			switch(status_code){
				case '0':
					break;
				case '1': /* Read the row ID */
					iResult = frecv(cnx->socket,(unsigned char*)&row_id,sizeof(row_id), 0);
					if (iResult == SOCKET_ERROR){
						free(colType);
						state->status = FOURD_ERROR;
						state->error_code = WSAGetLastError();
						sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
						return FOURD_ERROR;
					}

					/*
					if (DEBUG == 1)
						Printf("row_id:%d\n",row_id);
					 */

					len+=iResult;
					break;
				case '2':
					if (VERBOSE == 1)
						Printferr("Error during reading data\n");

					iResult = frecv(cnx->socket,(unsigned char*)&(state->error_code),sizeof(state->error_code), 0);
					if (iResult == SOCKET_ERROR){
						free(colType);
						state->status = FOURD_ERROR;
						state->error_code = WSAGetLastError();
						sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
						return FOURD_ERROR;
					}

					free(colType);
					state->status = FOURD_ERROR;
					state->error_code = FOURD_ERROR;
					sprintf_s(state->error_string,ERROR_STRING_LENGTH,"Error during reading data");
					return FOURD_ERROR;
				default:
					if (VERBOSE == 1) //FIXME
						Printferr("Status code 0x%X not supported in data at row %d column %d\n",
								  status_code,(elmts_offset-c+1)/nbCol+1,c+1);

					free(colType);
					state->status = FOURD_ERROR;
					state->error_code = FOURD_ERROR;
					sprintf_s(state->error_string,ERROR_STRING_LENGTH,"Status code not supported");
					return FOURD_ERROR;
			}
		}
		else {
			if (VERBOSE == 1)
				Printferr("Can not read the rowid\n");
		}

		// Read all columns
		for(c=0;c<nbCol;c++,elmts_offset++)
		{
			pElmt=&(state->elmt[elmts_offset]);
			pElmt->type=colType[c];

			// Read column status code
			status_code=0;

			iResult = frecv(cnx->socket,&status_code,sizeof(status_code), 0);
			if (iResult == SOCKET_ERROR){
				free(colType);
				state->status = FOURD_ERROR;
				state->error_code = WSAGetLastError();
				sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
				return FOURD_ERROR;
			}

			/*
			if (DEBUG == 1)
				Printf("Column status code: %2X\n",status_code);
			 */

			len+=iResult;
			switch(status_code)
			{
				case '2':// Error
					if (VERBOSE == 1)
						Printferr("Error during reading data\n");

					iResult = frecv(cnx->socket,(unsigned char*)&(state->error_code),sizeof(state->error_code), 0);
					if (iResult == SOCKET_ERROR){
						free(colType);
						state->status = FOURD_ERROR;
						state->error_code = WSAGetLastError();
						sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
						return FOURD_ERROR;
					}

					free(colType);
					state->status = FOURD_ERROR;
					state->error_code = FOURD_ERROR;
					sprintf_s(state->error_string,ERROR_STRING_LENGTH,"Error during reading data");
					return FOURD_ERROR;
				case '0':// Null value
					/*
					if (DEBUG == 1)
						Printf("Read a null value\n");
					 */

					pElmt->null=1;
					break;
				case '1':// Value
					pElmt->null=0;
					switch(colType[c])
					{
						case VK_BOOLEAN:
						case VK_BYTE:
						case VK_WORD:
						case VK_LONG:
						case VK_LONG8:
						case VK_REAL:
						case VK_DURATION:
							pElmt->pValue=calloc(1,(size_t)vk_sizeof(colType[c]));

							iResult = frecv(cnx->socket,(pElmt->pValue),vk_sizeof(colType[c]), 0);
							if (iResult == SOCKET_ERROR){
								free(colType);
								state->status = FOURD_ERROR;
								state->error_code = WSAGetLastError();
								sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
								return FOURD_ERROR;
							}
							len+=iResult;

							/*
							if (DEBUG == 1)
								Printf("Long: %d\n",*((int*)pElmt->pValue));
							 */

							break;
						case VK_TIMESTAMP:
						{
							FOURD_TIMESTAMP *tmp;
							tmp=calloc(1,sizeof(FOURD_TIMESTAMP));
							pElmt->pValue=tmp;

							iResult = frecv(cnx->socket,(unsigned char*)&(tmp->year),2, 0);
							if (iResult == SOCKET_ERROR){
								free(colType);
								state->status = FOURD_ERROR;
								state->error_code = WSAGetLastError();
								sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
								return FOURD_ERROR;
							}
							len+=iResult;

							/*
							if (DEBUG == 1)
								Printf("year: %04X",tmp->year);
							 */

							iResult = frecv(cnx->socket,(unsigned char*)&(tmp->mounth),1, 0);
							if (iResult == SOCKET_ERROR){
								free(colType);
								state->status = FOURD_ERROR;
								state->error_code = WSAGetLastError();
								sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
								return FOURD_ERROR;
							}
							len+=iResult;

							/*
							if (DEBUG == 1)
								Printf("    month: %02X",tmp->mounth);
							 */

							iResult = frecv(cnx->socket,(unsigned char*)&(tmp->day),1, 0);
							if (iResult == SOCKET_ERROR){
								free(colType);
								state->status = FOURD_ERROR;
								state->error_code = WSAGetLastError();
								sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
								return FOURD_ERROR;
							}
							len+=iResult;

							/*
							if (DEBUG == 1)
								Printf("    day: %02X",tmp->day);
							 */

							iResult = frecv(cnx->socket,(unsigned char*)&(tmp->milli),4, 0);
							if (iResult == SOCKET_ERROR){
								free(colType);
								state->status = FOURD_ERROR;
								state->error_code = WSAGetLastError();
								sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
								return FOURD_ERROR;
							}
							len+=iResult;

							/*
							if (DEBUG == 1)
								Printf("    milli: %08X\n",tmp->milli);
							 */

						}
							break;
						case VK_FLOAT:
						{
							FOURD_FLOAT *tmp;
							tmp=calloc(1,sizeof(FOURD_FLOAT));
							pElmt->pValue=tmp;

							iResult = frecv(cnx->socket,(unsigned char*)&(tmp->exp),4, 0);
							if (iResult == SOCKET_ERROR){
								free(colType);
								state->status = FOURD_ERROR;
								state->error_code = WSAGetLastError();
								sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
								return FOURD_ERROR;
							}
							len+=iResult;

							/*
							if (DEBUG == 1)
								Printf("Exp: %X",tmp->exp);
							 */

							iResult = frecv(cnx->socket,(unsigned char*)&(tmp->sign),1, 0);
							if (iResult == SOCKET_ERROR){
								free(colType);
								state->status = FOURD_ERROR;
								state->error_code = WSAGetLastError();
								sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
								return FOURD_ERROR;
							}
							len+=iResult;

							/*
							if (DEBUG == 1)
								Printf(" sign: %X",tmp->exp);
							 */

							iResult = frecv(cnx->socket,(unsigned char*)&(tmp->data_length),4, 0);
							if (iResult == SOCKET_ERROR){
								free(colType);
								state->status = FOURD_ERROR;
								state->error_code = WSAGetLastError();
								sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
								return FOURD_ERROR;
							}
							len+=iResult;

							/*
							if (DEBUG == 1)
								Printf(" length: %X",tmp->exp);
							 */

							tmp->data=calloc((size_t)tmp->data_length,1);
							iResult = frecv(cnx->socket,(tmp->data),tmp->data_length, 0);
							if (iResult == SOCKET_ERROR){
								free(colType);
								state->status = FOURD_ERROR;
								state->error_code = WSAGetLastError();
								sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
								return FOURD_ERROR;
							}
							len+=iResult;

							/*
							if (DEBUG == 1)
								Printf(" data: %X",tmp->data);
							 */

						}
							break;
						case VK_STRING:
						{
							int data_length=0;
							FOURD_STRING *str;
							// Read negative value of length of string
							str=calloc(1,sizeof(FOURD_STRING));
							pElmt->pValue=str;
							iResult = frecv(cnx->socket,(unsigned char*)&data_length,4, 0);
							if (iResult == SOCKET_ERROR){
								free(colType);
								state->status = FOURD_ERROR;
								state->error_code = WSAGetLastError();
								sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
								return FOURD_ERROR;
							}
							len+=iResult;

							/*
							if (DEBUG == 1)
								Printf("String length: %08X\n",data_length);
							 */

							data_length=-data_length;
							str->length=data_length;
							str->data=calloc((size_t)data_length*2+2,1);
							if(data_length==0){	// Correct read for empty string
								str->data[0]=0;
								str->data[1]=0;
							}
							else {
								iResult = frecv(cnx->socket,(str->data),(data_length*2), 0);
								if (iResult == SOCKET_ERROR){
									free(colType);
									state->status = FOURD_ERROR;
									state->error_code = WSAGetLastError();
									sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
									return FOURD_ERROR;
								}
								str->data[data_length*2]=0;
								str->data[data_length*2+1]=0;
								len+=iResult;
							}

						}
							break;
						case VK_IMAGE:
						{
							int data_length=0;
							FOURD_IMAGE *blob;
							blob=calloc(1,sizeof(FOURD_IMAGE));
							pElmt->pValue=blob;

							iResult = frecv(cnx->socket,(unsigned char*)&data_length,4, 0);
							if (iResult == SOCKET_ERROR){
								free(colType);
								state->status = FOURD_ERROR;
								state->error_code = WSAGetLastError();
								sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
								return FOURD_ERROR;
							}
							len+=iResult;

							/*
							if (DEBUG == 1)
								Printf("Image length: %08X\n",data_length);
							 */

							if(data_length==0){
								blob->length=0;
								blob->data=NULL;
								pElmt->null=1;
							}else{
								blob->length=data_length;
								blob->data=calloc((size_t)data_length,1);
								iResult = frecv(cnx->socket,blob->data,data_length, 0);
								if (iResult == SOCKET_ERROR){
									free(colType);
									state->status = FOURD_ERROR;
									state->error_code = WSAGetLastError();
									sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
									return FOURD_ERROR;
								}
								len+=iResult;
							}

							/*
							if (DEBUG == 1)
								Printf("Image: %d bytes\n",data_length);
							 */

						}
							break;
						case VK_BLOB:
						{
							int data_length=0;
							FOURD_BLOB *blob;
							blob=calloc(1,sizeof(FOURD_BLOB));
							pElmt->pValue=blob;

							iResult = frecv(cnx->socket,(unsigned char*)&data_length,4, 0);
							if (iResult == SOCKET_ERROR){
								free(colType);
								state->status = FOURD_ERROR;
								state->error_code = WSAGetLastError();
								sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
								return FOURD_ERROR;
							}
							len+=iResult;

							/*
							if (DEBUG == 1)
								Printf("Blob length: %08X\n",data_length);
							 */

							if(data_length==0){
								blob->length=0;
								blob->data=NULL;
								pElmt->null=1;
							}else{
								blob->length=data_length;
								blob->data=calloc(data_length,1);
								iResult = frecv(cnx->socket,blob->data,data_length, 0);
								if (iResult == SOCKET_ERROR){
									free(colType);
									state->status = FOURD_ERROR;
									state->error_code = WSAGetLastError();
									sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
									return FOURD_ERROR;
								}
								len+=iResult;
							}

							/*
							if (DEBUG == 1)
								Printf("Blob: %d bytes\n",data_length);
							 */

						}
							break;
						default:
							if (VERBOSE == 1)
								Printferr("Type not supported (%s) at row %d column %d\n",
										  stringFromType(colType[c]),(elmts_offset-c+1)/nbCol+1,c+1);

							free(colType);
							state->status = FOURD_ERROR;
							state->error_code = FOURD_ERROR;
							sprintf_s(state->error_string,ERROR_STRING_LENGTH,"Type not supported");
							return FOURD_ERROR;
					}
					break;
				default:
					if (VERBOSE == 1)
						Printferr("Status code 0x%X not supported in data at row %d column %d\n",
								  status_code,(elmts_offset-c+1)/nbCol+1,c+1);

					free(colType);
					state->status = FOURD_ERROR;
					state->error_code = FOURD_ERROR;
					sprintf_s(state->error_string,ERROR_STRING_LENGTH,"Status code not supported");
					return FOURD_ERROR;
			}
		}
	}

	/*
	if (DEBUG == 1)
		Printf("End debugging the socket_receiv_data()\n");
	 */

	free(colType);
	return FOURD_OK;
}

/* Receives update count from the 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_receiv_update_count(FOURD *cnx,FOURD_RESULT *state)
{
	FOURD_LONG8 data=0;
	long iResult = 0;

	if(cnx->connected == 0)
	{
		if (VERBOSE == 1)
			Printferr("Socket not connected\n");

		return FOURD_ERROR;
	}

	iResult = frecv(cnx->socket,(unsigned char*)&data,8, 0);
	if (iResult == SOCKET_ERROR){
		state->status = FOURD_ERROR;
		state->error_code = WSAGetLastError();
		sprintf_s(state->error_string,ERROR_STRING_LENGTH,strerror(WSAGetLastError()));
		return FOURD_ERROR;
	}

	if (DEBUG == 1)
		Printf("Update-Count: Ox%X\n",data);

	cnx->updated_row=data;

	if (DEBUG == 1)
		Printf("\n");

	return FOURD_OK;
}

/* Sets socket blocking */
int set_sock_blocking(int socketd, int block)
{
	int ret = 0;
	int flags;
	int myflag = 0;

#ifdef WIN32
	/* With ioctlsocket, a non-zero sets nonblocking, a zero sets blocking */
	flags = !block;
	if (ioctlsocket(socketd, FIONBIO, &flags) == SOCKET_ERROR)
		ret = 1;
#else
	flags = fcntl(socketd, F_GETFL);
#ifdef O_NONBLOCK
	myflag = O_NONBLOCK; /* POSIX version */
#elif defined(O_NDELAY)
	myflag = O_NDELAY;   /* Old non-POSIX version */
#endif
	if (!block) {
		flags |= myflag;
	} else {
		flags &= ~myflag;
	}
	fcntl(socketd, F_SETFL, flags);
#endif

	return ret;
}

/* Opens a socket connection to 4D server with a timeout flag
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_connect_timeout(FOURD *cnx,const char *host,unsigned int port,int timeout)
{
	struct addrinfo *result = NULL, *ptr = NULL, hints;
	int iResult = 0, valopt =0 ;
	struct timeval tv;
	fd_set myset;
	socklen_t lon;
	int flag = 0;

	char sport[50];
	sprintf_s(sport,50,"%d",port);

	// Initialize Hints
	ZeroMemory( &hints, sizeof(hints) );
	hints.ai_family = AF_INET; // IPv4
	hints.ai_socktype = SOCK_STREAM; // TCP
	hints.ai_protocol = IPPROTO_TCP; // TCP

	// Resolve the server address and port
	iResult = getaddrinfo(host, sport, &hints, &result);
	if ( iResult != 0 ) {
		if (VERBOSE == 1)
			Printferr("Error in socket_connect() -> getaddrinfo() failed with error: %d, %s\n",
					  iResult,gai_strerror(iResult));

		cnx->status = FOURD_ERROR;
		cnx->error_code = iResult;
		sprintf_s(cnx->error_string,ERROR_STRING_LENGTH,gai_strerror(iResult));
		freeaddrinfo(result);
		return FOURD_ERROR;
	}


	// Try to create a socket and connect getaddrinfo can
	// return multiple structures try all
	flag=1;
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next){

		iResult = 0;

		// Create a socket to connect to the 4D server
		cnx->socket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (cnx->socket != INVALID_SOCKET){

			// If we get an error here, we can safely ignore it. The connection may be slower, but it should
			// still work.
			setsockopt(cnx->socket, IPPROTO_TCP, TCP_NODELAY, (char *)&flag, sizeof(int));

			// Set Non blocking socket
			set_sock_blocking(cnx->socket,0);

			iResult = connect( cnx->socket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR){
                if (WSAGetLastError() == EINPROGRESS) {
					tv.tv_sec = timeout;
					tv.tv_usec = 0;
					FD_ZERO(&myset);
					FD_SET(cnx->socket, &myset);

					if (select(cnx->socket+1, NULL, &myset, NULL, &tv) > 0) {

						lon = sizeof(int);
						getsockopt(cnx->socket, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &lon);

						if (valopt)
						{
							closesocket(cnx->socket);
							cnx->socket = INVALID_SOCKET;
						}
						iResult = 0;
						break; /* Connection OK */
					}
					else{
						closesocket(cnx->socket);
						cnx->socket = INVALID_SOCKET;
					}

                }
                else{
                    closesocket(cnx->socket);
					cnx->socket = INVALID_SOCKET;
                }
			}else{
				iResult = 0;
				break; /* Connection OK */
			}

		}

	}


	// Invalid socket to 4D server
	if (cnx->socket == INVALID_SOCKET) {
		if (VERBOSE == 1)
			Printferr("Error in socket_connect() -> socket() failed with error: %ld\n", WSAGetLastError());

		cnx->status = FOURD_ERROR;
		cnx->error_code = WSAGetLastError();
		sprintf_s(cnx->error_string,ERROR_STRING_LENGTH,"Unable to create a socket to the 4D server");
		freeaddrinfo(result);
		return FOURD_ERROR;
	}

	// Couldn't connect to 4D server
	if (iResult == SOCKET_ERROR) {

		if (VERBOSE == 1)
			Printferr("Error in socket_connect() -> connect() failed with error: %d\n", WSAGetLastError());

		cnx->status = FOURD_ERROR;
		cnx->error_code = WSAGetLastError();
		sprintf_s(cnx->error_string,ERROR_STRING_LENGTH,"Unable to connect to the 4D server");
		freeaddrinfo(result);
		closesocket(cnx->socket);
		cnx->socket = INVALID_SOCKET;
		return FOURD_ERROR;
	}

	// Set blocking socket because 4D data is bit streamed
	set_sock_blocking(cnx->socket,1);

	freeaddrinfo(result);

	return FOURD_OK;
}
