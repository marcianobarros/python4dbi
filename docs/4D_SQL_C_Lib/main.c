#include <stdio.h>
#include "fourd.h"
#include "fourd_int.h"
#include "communication.h"
#include "utils.h"
#include <signal.h>
#include "string.h"
#include <time.h>

int main() {

    FOURD *cnx = NULL;

    int error = 0;
    unsigned int port = 19812;
    char *host = "127.0.0.1";
    char *user = "theUser";
    char *password = "thePassword";
    char *base = "";
    FOURD_STATEMENT *statement = NULL;
    FOURD_RESULT *result = NULL;

    clock_t start, end;
    double cpu_time_used;

    char *query = "SELECT EmployeeID, Address1, Address2, City FROM EMPLOYEES";


    //Create the socket struct
    cnx = fourd_init();

    start = clock();
    //Open connection to 4D and login
    error = fourd_connect(cnx, host, user, password, base, port);
    if(error == FOURD_OK){

        //Prepare the statement struct
        statement = fourd_prepare_statement(cnx, query);

        //Execute the statement
        result = fourd_exec_statement(statement, PAGE_SIZE);

        if(result != NULL){

            // Fetch the content rows
            for (int i = 0; i <= result->row_count; ++i) {
                error = fourd_next_row(result);

                if(error == FOURD_ERROR){
                    //Handle error
                }
            }

            fourd_free_result(result);
            fourd_close(cnx);

        }else{
            //Handle the error
        }


    }else{
        // Handle the error
    }
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Execution time : %f seconds \n", cpu_time_used);

    printf("Press enter to continue...\n");
    getchar();

    return 0;
}