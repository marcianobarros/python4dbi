#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#define isspace(x) (x==' ')

/* Variable argument print function */
int Printf(const char* format,...)
{
    va_list ap;
    va_start(ap,format);
    vprintf(format,ap);

    return 0;
}

/* Variable argument print error function */
int Printferr(const char* format,...)
{
    va_list ap;
    va_start(ap,format);
    vfprintf(stderr,format,ap);

    return 0;
}

/* String strip */
char *str_strip(char *s)
{
       size_t size;
       char *end;

       size = strlen(s);

       if (!size)
               return s;

       end = s + size - 1;
       while (end != s && isspace(*end))
               end--;
       *(end + 1) = '\0';

       while (*s && isspace(*s))
               s++;

       return s;
}

/* WIN32 functions */
#ifndef WIN32

/* ZeroMemory macro fills a block of memory with zeros */
void ZeroMemory (void *s, size_t n)
{
    bzero(s,n);
}

/* Write formatted data to a string */
int sprintf_s(char *buff,size_t size,const char* format,...)
{
    va_list ap;
    va_start(ap,format);
    vsnprintf(buff,size,format,ap);

    return 0;
}

/* Write formatted data to a string */
int _snprintf_s(char *buff, size_t size, size_t count, const char *format,...)
{
    va_list ap;
    va_start(ap,format);
    vsnprintf(buff,((size>count)?count:size),format,ap);

    return 0;
}

/* Write formatted data to a string */
int _snprintf(char *buff, int size, const char *format,...)
{
    va_list ap;
    va_start(ap,format);
    vsnprintf(buff,size,format,ap);

    return 0;
}

#endif /* WIN32 */