#ifndef __FOURD__
#define __FOURD__ 1

#define VERBOSE 0
#define DEBUG 0
#define SOCKET_TIMEOUT 15
#define MAX_COL_TYPES_LENGHT 4096
#define ERROR_STRING_LENGTH 2048
#define HEADER_GROW_SIZE 1024
#define DEFAULT_IMAGE_TYPE "jpg"
#define MAX_LENGTH_COLUMN_NAME 255
#define MAX_LENGTH_COLUMN_TYPE 50
#define MAX_STRING_NUMBER 255
#define FOURD_OK 0
#define FOURD_ERROR 1
#define STATEMENT_BASE64 0
#define LOGIN_BASE64 0
#define PROTOCOL_VERSION "12.0"
#define PAGE_SIZE 1
#define OUTPUT_MODE "release"

#include <stdio.h> /* size_t */

/* FOUR_TYPE's */
typedef enum
{
	VK_UNKNOW=0,
	VK_BOOLEAN,
	VK_BYTE,
	VK_WORD,
	VK_LONG,
	VK_LONG8,
	VK_REAL,
	VK_FLOAT,
	VK_TIME,
	VK_TIMESTAMP,
	VK_DURATION,
	VK_TEXT,
	VK_STRING,
	VK_BLOB,
	VK_IMAGE
}FOURD_TYPE;

/* FOURD_RESULT_TYPE's */
typedef enum
{
	UNKNOW=0,
	UPDATE_COUNT,
	RESULT_SET
}FOURD_RESULT_TYPE;

/* Structure of VK_* */
typedef short FOURD_BOOLEAN;
typedef short FOURD_BYTE;
typedef short FOURD_WORD;
typedef int FOURD_LONG;
#ifdef WIN32
typedef	__int64 FOURD_LONG8;
#else
typedef long long FOURD_LONG8;
#endif /* WIN32 */
typedef	double FOURD_REAL;
typedef	struct{
	int exp;
	char sign;
	int data_length;
	void* data; /* variable length */
}FOURD_FLOAT;
typedef	struct{
	short year;
	char mounth;
	char day;
	unsigned int milli;
}FOURD_TIMESTAMP;
#ifdef WIN32
typedef	__int64 FOURD_DURATION; // in milliseconds
#else
typedef long long FOURD_DURATION; // in milliseconds
#endif /* WIN32 */
typedef struct{
	int length;
	unsigned char *data; /* variable length */
}FOURD_STRING;
typedef struct{
	int length;
	void *data; /* variable length */
}FOURD_BLOB;
typedef struct{
	int length;
	void *data; /* variable length */
}FOURD_IMAGE;

/* 4D Connection struct */
typedef struct{

	/* Socket Win32 */
#ifdef WIN32
	WSADATA wsaData;
	SOCKET socket;
#else
	int socket;
#endif /* WIN32 */

	int init; /*boolean*/
	int connected; /*boolean*/

	/* Status of the connection (OK or KO) */
	int status;	/* FOURD_OK or FOURD_ERRROR */
	FOURD_LONG8 error_code;
	char error_string[ERROR_STRING_LENGTH];

	/* Updated row */
	FOURD_LONG8 updated_row;

	/* Command number used for */
	/* LOGIN, STATEMENT, ETC*/
	unsigned int id_cnx;

	/* PREFERRED-IMAGE-TYPES */
	char *preferred_image_types;

	/* Connection timeout */
	int timeout;

}FOURD;

typedef struct{
	FOURD_TYPE type;
	char null; // 0 not null, 1 null
	void *pValue;
}FOURD_ELEMENT;

typedef struct{
	char sType[MAX_LENGTH_COLUMN_TYPE];
	FOURD_TYPE type;
	char sColumnName[MAX_LENGTH_COLUMN_NAME];
}FOURD_COLUMN;

typedef struct{
	unsigned int nbColumn;
	FOURD_COLUMN *Column;
}FOURD_ROW_TYPE;

typedef struct{
	FOURD *cnx;
	char *header;
	unsigned int header_size;

	/* Status of the statement (OK or KO) */
	int status;	/* FOURD_OK or FOURD_ERRROR */
	FOURD_LONG8 error_code;
	char error_string[ERROR_STRING_LENGTH];

	/*result of parse header
	  RESULT_SET for select
	  UPDATE_COUNT for insert, update, delete
	  */
	FOURD_RESULT_TYPE resultType;

	/* ID of the statement used with 4D SQL-server */
	int id_statement;

	/*Id of the command use for request */
	int id_command;

	/* Updateability is true or false */
	int updateability;

	/* Total of rows of the result query */
	unsigned int row_count;

	/* row count in data buffer aka page size */
	unsigned int row_count_sent;

	/*num of the first row
	with default parameter on 4D server: 0 */
	unsigned int first_row;

	/* row_type of this statement
	   contains column count, column name and column type*/
	FOURD_ROW_TYPE row_type;

	/*data*/
	FOURD_ELEMENT *elmt;

	/*current row index*/
	int numRow;

}FOURD_RESULT;

typedef struct {
	FOURD *cnx;
	char *query;	/*MAX_HEADER_SIZE is using because the query is insert into header*/
	unsigned int nb_element;
	unsigned int nbAllocElement;
	FOURD_ELEMENT *elmt;
	/* PREFERRED-IMAGE-TYPES */
	char *preferred_image_types;
}FOURD_STATEMENT;

/* Returns the FOURD_TYPE enum from a str */
FOURD_TYPE typeFromString(const char *type);

/* Returns the FOURD_TYPE str from an enum */
const char* stringFromType(FOURD_TYPE type);

/* Return sizeof FOURD_TYPE or -1 if variable length or 0 if unknown type */
int vk_sizeof(FOURD_TYPE type);

/* Returns the FOURD_RESULT_TYPE enum from a str */
FOURD_RESULT_TYPE resultTypeFromString(const char *type);

/* Returns the FOURD_RESULT_TYPE str from an enum */
const char* stringFromResultType(FOURD_RESULT_TYPE type);

/* Sets the cnx PREFERRED-IMAGE-TYPES */
const char* fourd_get_preferred_image_types(FOURD* cnx);

/* Returns a FOURD connection struct initialized */
FOURD* fourd_init(void);

/* Connects the cnx to 4D Server with LOGIN
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int fourd_connect(FOURD *cnx,const char *host,const char *user,const char *password,const char *base,unsigned int port);

/* Frees the cnx struct */
int fourd_close(FOURD *cnx);

/* Frees the cnx struct */
void fourd_free(FOURD* cnx);

/* Executes a query command no data is return
 * The default PAGE_SIZE is used
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int fourd_exec(FOURD *cnx,const char *query);

/* Get the number of number affected rows */
FOURD_LONG8 fourd_affected_rows(FOURD *cnx);

/* Get the connection error number */
int fourd_errno(FOURD *cnx);

/* Get the connection error string */
const char * fourd_error(FOURD *cnx);

/* Get the total number of rows of the result query */
FOURD_LONG8 fourd_num_rows(FOURD_RESULT *result);

/* Executes a query command returns
 * the result data struct
 * The default PAGE_SIZE is used */
FOURD_RESULT *fourd_query(FOURD *cnx,const char *query);

/* Executes a query command returns
 * the result data struct
 * The PAGE_SIZE can be set */
FOURD_RESULT *fourd_exec_statement(FOURD_STATEMENT *state, int res_size);

/* Clears FOURD_RESULT */
void fourd_free_result(FOURD_RESULT *res);

/* Prepares and returns the FOURD_STATEMENT */
FOURD_STATEMENT * fourd_prepare_statement(FOURD *cnx,const char *query);

/* Clears FOURD_STATEMENT */
void fourd_free_statement(FOURD_STATEMENT *state);

/* Closes the STATEMENT
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int fourd_close_statement(FOURD_RESULT *res);

/* Closes the next result page
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int fourd_next_row(FOURD_RESULT *res);

/* Sets the connection timeout */
void fourd_timeout(FOURD* cnx,int timeout);

/* Returns field casted to FOURD_LONG */
FOURD_LONG * fourd_field_long(FOURD_RESULT *res,unsigned int numCol);

/* Returns field casted to FOURD_STRING */
FOURD_STRING * fourd_field_string(FOURD_RESULT *res,unsigned int numCol);

/* Returns field */
void * fourd_field(FOURD_RESULT *res,unsigned int numCol);

/* Converts a fourd element value to a string
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int fourd_field_to_string(FOURD_RESULT *res,unsigned int numCol,char **value,size_t *len);

/* Returns the col name */
const char * fourd_get_column_name(FOURD_RESULT *res,unsigned int numCol);

/* Returns the col type */
FOURD_TYPE fourd_get_column_type(FOURD_RESULT *res,unsigned int numCol);

/* Returns numnber o columns */
int fourd_num_columns(FOURD_RESULT *res);

/* Returns an allocated FOURD_STRING */
FOURD_STRING *fourd_create_string(char *param,int length);

int fourd_bind_param(FOURD_STATEMENT *state,unsigned int numParam,FOURD_TYPE type, void *val);

void fourd_set_preferred_image_types(FOURD* cnx,const char *types);

void fourd_set_statement_preferred_image_types(FOURD_STATEMENT *state,const char *types);

const char* fourd_get_statement_preferred_image_types(FOURD_STATEMENT *state);

#endif /* __FOURD__ */
