#ifndef __FOURD_INT__
#define __FOURD_INT__ 1

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Returns len of N */
unsigned int ndigits(unsigned int n);

/* Clears connection attributes */
void _clear_atr_cnx(FOURD *cnx);

/* Sends LOGIN msg to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int dblogin(FOURD *cnx,unsigned int id_cnx,const char *user,const char*pwd,const char*image_type);

/* Sends LOGOUT msg to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int dblogout(FOURD *cnx,unsigned int id_cnx);

/* Sends QUIT msg to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int quit(FOURD *cnx,unsigned int id_cnx);

/* Checks if a request is multiquery
 * Returns 0 = Is not MultiQuery , 1 = Is MultiQuery
 * $query  = "SELECT CURRENT_USER();
 * $query = $query + "SELECT Name FROM City ORDER BY ID LIMIT 20, 5"; */
int _is_multi_query(const char *request);

/* Checks if a request is multiquery
 * Returns FOURD_OK = Is not MultiQuery , FOURD_ERROR = Is MultiQuery */
int _valid_query(FOURD *cnx,const char *request);

/* Sends EXECUTE-STATEMENT msg to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int _prepare_statement(FOURD *cnx,unsigned int id_cnx,const char *request);

/* Sends EXECUTE-STATEMENT msg to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int _query(FOURD *cnx,unsigned int id_cnx,const char *request,FOURD_RESULT *result,const char*image_type, int res_size);

/* Sends EXECUTE-STATEMENT msg with bind parameters to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int _query_param(FOURD *cnx,unsigned int id_cnx, const char *request,unsigned int nbParam, const FOURD_ELEMENT *param,
                 FOURD_RESULT *result,const char*image_type,int res_size);

/* Get next row set in result_set
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int _fetch_result(FOURD_RESULT *res,unsigned int id_cnx);

/* Low level command
   command_index and statement_id is identify by result of teh execute statement command
   Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int __fetch_result(FOURD *cnx,unsigned int id_cnx,int statement_id,int command_index,unsigned int first_row,
        unsigned int last_row,FOURD_RESULT *result);

/* Finds a the value of a header section for example
 * msg => Error-Description : Dummy error :> the value is Dummy error
 * get(header,"Error-Description",error_string,ERROR_STRING_LENGTH);
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int get(const char* msg,const char* section,char *value,size_t max_length);

/* Replace '] [' by ']\r[' in Column-Aliases-Base64 list header
 * to be splitted by token '\r'*/
void _alias_str_replace(char *list_alias);

/* Treats the header part of the response after a sending a msg
 * to the 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int treat_header_response(FOURD_RESULT* cnx);

/* Get the response status OK or ERROR?
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
FOURD_LONG8 _get_status(const char *header,int *status,FOURD_LONG8 *error_code,char *error_string);

/* Check the response
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int receiv_check(FOURD *cnx,FOURD_RESULT *state);

/* Sends CLOSE-STATEMENT msg to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int close_statement(FOURD_RESULT *res,unsigned int id_cnx);

/* Frees memory allocated by FOURD_RESULT struct */
void _free_data_result(FOURD_RESULT *res);

/* Frees memory allocated by FOURD_STATEMENT struct */
void _free_fourd_statement_elmts(FOURD_STATEMENT *state);

/* Memory Allocation */
void *_copy(FOURD_TYPE type,void *org);
char *_serialize(char *data,unsigned int *size, FOURD_TYPE type, void *pObj);
void Free(void *p);
void FreeFloat(FOURD_FLOAT *p);
void FreeString(FOURD_STRING *p);
void FreeBlob(FOURD_BLOB *p);
void FreeImage(FOURD_IMAGE *p);
void PrintData(const void *data,size_t size);



#endif /* __FOURD_INT__ */
