#ifndef __SQLSTATE_H__
#define __SQLSTATE_H__ 1

#include "fourd.h"

/* Returns the 4D SQL server error string */
const char * fourd_sqlstate(FOURD *cnx);

#endif /* __SQLSTATE_H__ */
