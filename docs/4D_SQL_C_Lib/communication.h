#ifndef __COMMUNICATION_H__
#define __COMMUNICATION_H__ 1

#include "fourd.h"

/* The WSAGetLastError function returns the error status for the last Windows Sockets operation that failed
 * In Winsock applications, error codes are retrieved using the WSAGetLastError function,
 * the Windows Sockets substitute for the Windows GetLastError function.
 * The error codes returned by Windows Sockets are similar to UNIX socket error code constants,
 * but the constants are all prefixed with WSA. So in Winsock applications the WSAEWOULDBLOCK
 * error code would be returned, while in UNIX applications the EWOULDBLOCK error code would be returned.*/

#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#include <Wspiapi.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <errno.h>
#define WSAGetLastError() errno
#include <netdb.h> /* gethostbyname */
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;
#endif /* WIN32 */

/* The function receives data from a connected 4D server socket
 * or a bound connectionless socket.
 * Uses recv() */
long frecv(SOCKET s,unsigned char *buf,int len,int flag);

/* Opens a socket connection to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_connect(FOURD *cnx,const char *host,unsigned int port);

/* Closes a socket connection to 4D server */
void socket_disconnect(FOURD *cnx);

/* Sends a msg to the 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_send(FOURD *cnx,const char*msg);

/* Sends a msg to the 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_send_data(FOURD *cnx,const char*msg,size_t len);

/* Receives a msg header from the 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_receiv_header(FOURD *cnx,FOURD_RESULT *state);

/* Receives data from the 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_receiv_data(FOURD *cnx,FOURD_RESULT *state);

/* Receives update count from the 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_receiv_update_count(FOURD *cnx,FOURD_RESULT *state);

/* Sets socket blocking */
int set_sock_blocking(int socketd, int block);

/* Opens a socket connection to 4D server with a timeout flag
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int socket_connect_timeout(FOURD *cnx,const char *host,unsigned int port,int timeout);

#endif /* __COMMUNICATION_H__ */