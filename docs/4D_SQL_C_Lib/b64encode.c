#include <stdio.h>
#include <stdlib.h>
#include "b64.h"

#ifdef b64_USE_CUSTOM_MALLOC
extern void* b64_malloc(size_t);
#endif

#ifdef b64_USE_CUSTOM_REALLOC
extern void* b64_realloc(void*, size_t);
#endif

unsigned char *
b64_encode (const char *src, size_t len) {
  int i = 0;
  int j = 0;
  char *enc = NULL;
  size_t size = 0;
  char buf[4];
  char tmp[3];

  // Alloc
  enc = (char *) b64_malloc(1);
  if (NULL == enc){
    free(enc);
    return NULL;
  }

  // Parse until end of source
  while (len--) {
    // Read up to 3 bytes at a time into `tmp'
    tmp[i++] = *(src++);

    // If 3 bytes read then encode into `buf'
    if (3 == i) {
      buf[0] = (char) ((tmp[0] & 0xfc) >> 2);
      buf[1] = (char) (((tmp[0] & 0x03) << 4) + ((tmp[1] & 0xf0) >> 4));
      buf[2] = (char) (((tmp[1] & 0x0f) << 2) + ((tmp[2] & 0xc0) >> 6));
      buf[3] = (char) (tmp[2] & 0x3f);

      // Allocate 4 new bytes for `enc` and
      // then translate each encoded buffer
      // part by index from the base 64 index table
      // into `enc' unsigned char array
      enc = (char *) b64_realloc(enc, size + 4);
      for (i = 0; i < 4; ++i) {
        enc[size++] = b64_table[buf[i]];
      }

      // Reset index
      i = 0;
    }
  }

  // Remainder
  if (i > 0) {
    // Fill `tmp' with `\0' at most 3 times
    for (j = i; j < 3; ++j) {
      tmp[j] = '\0';
    }

    // Perform same codec as above
    buf[0] = (char) ((tmp[0] & 0xfc) >> 2);
    buf[1] = (char) (((tmp[0] & 0x03) << 4) + ((tmp[1] & 0xf0) >> 4));
    buf[2] = (char) (((tmp[1] & 0x0f) << 2) + ((tmp[2] & 0xc0) >> 6));
    buf[3] = (char) (tmp[2] & 0x3f);

    // Perform same write to `enc` with new allocation
    for (j = 0; (j < i + 1); ++j) {
      enc = (char *) b64_realloc(enc, size + 1);
      enc[size++] = b64_table[buf[j]];
    }

    // While there is still a remainder
    // append `=' to `enc'
    while ((i++ < 3)) {
      enc = (char *) b64_realloc(enc, size + 1);
      enc[size++] = '=';
    }
  }

  // Make sure we have enough space to add '\0' character at end.
  enc = (char *) b64_realloc(enc, size + 1);
  enc[size] = '\0';

  return (unsigned char*) enc;
}
