#include "fourd.h"
#include "fourd_int.h"
#include "b64.h"
#include "utils.h"
#include "communication.h"
#include <math.h>
#include <string.h>

/* Returns len of N */
unsigned int ndigits(unsigned int n){
    return ( n==0 ) ? 1 : (int)log10(n) + 1;
}

/* Clears connection attributes */
void _clear_atr_cnx(FOURD *cnx)
{
    cnx->status = 0;
    cnx->error_code = 0;
    strcpy_s(cnx->error_string,ERROR_STRING_LENGTH,"");
    cnx->updated_row = 0;
}

/* Sends LOGIN msg to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int dblogin(FOURD *cnx,unsigned int id_cnx,const char *user,const char*pwd,const char*image_type)
{
    char *msg = NULL;
    char *format_str = NULL;
    size_t buff_size = 0;
    FOURD_RESULT state;
    unsigned char *user_b64 = NULL, *pwd_b64 = NULL;
    _clear_atr_cnx(cnx);

#if LOGIN_BASE64
    user_b64=b64_encode(user,strlen(user));
	pwd_b64=b64_encode(pwd,strlen(pwd));
	format_str = "%d LOGIN \r\nUSER-NAME-BASE64:%s\r\nUSER-PASSWORD-BASE64:%s\r\n"
              "PREFERRED-IMAGE-TYPES:%s\r\nREPLY-WITH-BASE64-TEXT:Y\r\nPROTOCOL-VERSION:%s\r\n\r\n");
    buff_size = strlen(format_str) + ndigits(id_cnx) + strlen((const char*)user_b64)
            + strlen((const char*)pwd_b64) + strlen(image_type) + strlen((const char*)PROTOCOL_VERSION);
    msg = (char *)malloc(buff_size);
	sprintf_s(msg,buff_size,format_str,id_cnx,user_b64,pwd_b64,image_type,PROTOCOL_VERSION);
	Free(user_b64);
	Free(pwd_b64);
#else
    format_str = "%d LOGIN \r\nUSER-NAME:%s\r\nUSER-PASSWORD:%s\r\n"
                 "PREFERRED-IMAGE-TYPES:%s\r\nREPLY-WITH-BASE64-TEXT:Y\r\nPROTOCOL-VERSION:%s\r\n\r\n";
    buff_size = strlen(format_str) + ndigits(id_cnx) + strlen(user) +
            strlen(pwd) + strlen(image_type) + strlen((const char*)PROTOCOL_VERSION);
    msg = (char *)malloc(buff_size);
    sprintf_s(msg,buff_size,format_str,id_cnx,user,pwd,image_type,PROTOCOL_VERSION);
#endif

    if(socket_send(cnx,msg) == FOURD_ERROR){
        Free(msg);
        return FOURD_ERROR;
    }

    Free(msg);

    if(receiv_check(cnx,&state) == FOURD_ERROR)
        return FOURD_ERROR;

    return FOURD_OK;
}

/* Sends LOGOUT msg to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int dblogout(FOURD *cnx,unsigned int id_cnx)
{
    char *msg = NULL;
    char *format_str = NULL;
    size_t buff_size = 0;
    FOURD_RESULT state;
    _clear_atr_cnx(cnx);

    format_str = "%d LOGOUT\r\n\r\n";
    buff_size = strlen(format_str) + ndigits(id_cnx);
    msg = (char *)malloc(buff_size);
    sprintf_s(msg,buff_size,format_str,id_cnx);

    if(socket_send(cnx,msg) == FOURD_ERROR){
        Free(msg);

        return FOURD_ERROR;
    }

    Free(msg);

    if(receiv_check(cnx,&state) == FOURD_ERROR)
        return FOURD_ERROR;

    return FOURD_OK;
}

/* Sends QUIT msg to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int quit(FOURD *cnx,unsigned int id_cnx)
{
    char *msg = NULL;
    char *format_str = NULL;
    size_t buff_size = 0;
    FOURD_RESULT state;
    _clear_atr_cnx(cnx);

    format_str = "%d QUIT\r\n\r\n";
    buff_size = strlen(format_str) + ndigits(id_cnx);
    msg = (char *)malloc(buff_size);
    sprintf_s(msg,buff_size,format_str,id_cnx);

    if(socket_send(cnx,msg) == FOURD_ERROR){
        Free(msg);

        return FOURD_ERROR;
    }

    Free(msg);

    if(receiv_check(cnx,&state) == FOURD_ERROR)
        return FOURD_ERROR;

    return FOURD_OK;
}

/* Checks if a request is multiquery
 * Returns FOURD_OK = Is not MultiQuery , FOURD_ERROR = Is MultiQuery
 * $query  = SELECT * from [Clientes];
 * $query = $query + SELECT * from [Contratos]; */
int _is_multi_query(const char *request)
{
    int i = 0;
    size_t len = 0;
    int inCol = 0;
    int inStr = 0;
    int finFirst = 0;
    char car = 0;

    if(request==NULL){
        return FOURD_OK;
    }

    len=strlen(request);
    if(len<1){
        return FOURD_OK;
    }

    for(i=0;i<len;i++){

        car=request[i];

        switch(car){
            case '[':
                /* start of 4D object name */
                if(!inStr){
                    if(!inCol){
                        /* printf("["); */
                        inCol=1;
                    }
                    else {
                        /* printf("_"); */
                    }
                }else {
                    /* printf("s"); */
                }
                break;
            case ']':
                if(inStr){
                    /* printf("s"); */
                }else if(inCol){
                    inCol=0;
                    /* printf("]"); */
                }else {
                    if(i>1){ /* check the previous character */
                        if(request[i-1]==']'){
                            /* not end of colomn name */
                            inCol=1;
                            /* printf("-"); */
                        }else {
                            inCol=0;
                            /* printf("]"); */
                        }
                    }else {
                        /* printf("_");*/
                    }
                }

                break;
            case '\'':
                if(!inCol){
                    /* printf("'");*/
                    if(inStr==0){
                        inStr=1;
                    }else{
                        inStr=0;
                    }
                }else{
                    /* printf("c"); */
                }
                break;
            case ';':
                /* end of query */
                if(!inCol && !inStr){
                    finFirst=1;
                    /* printf(";");*/
                }else {
                    /*printf("_");*/
                }
                break;
            default:
                if(inCol){
                    /* printf("C"); */
                }
                else if(inStr){
                    /* printf("S"); */
                }
                else if(car==' '){
                    /*printf(" ");*/
                }else{
                    if(finFirst){
                        /* printf("X"); */
                        return FOURD_ERROR;
                    }else {
                        /* printf("*"); */
                    }
                }
                break;
        }

    }

    return FOURD_OK;
}

/* Checks if a request is multiquery
 * Returns FOURD_OK = Is not MultiQuery , FOURD_ERROR = Is MultiQuery */
int _valid_query(FOURD *cnx,const char *request)
{
    if(_is_multi_query(request) == FOURD_ERROR){
        cnx->error_code = -5001;
        sprintf_s(cnx->error_string,ERROR_STRING_LENGTH,"MultiQuery not supported",ERROR_STRING_LENGTH);
        return FOURD_ERROR;
    }

    return FOURD_OK;
}

/* Sends PREPARE-STATEMENT msg to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int _prepare_statement(FOURD *cnx,unsigned int id_cnx,const char *request)
{
    char *msg=NULL;
    FOURD_RESULT *res=calloc(1,sizeof(FOURD_RESULT));


#if STATEMENT_BASE64
    unsigned char *request_b64=NULL;
	request_b64=b64_encode(request,strlen(request));
	char *format_str="%d PREPARE-STATEMENT\r\nSTATEMENT-BASE64: %s\r\n\r\n";
	unsigned long buff_size=strlen(format_str)+strlen((const char *)request_b64)+2; //add some extra for good measure.
	msg=(char *)malloc(buff_size);
	snprintf(msg,buff_size,format_str,id_cnx,request_b64);
	Free(request_b64);
#else
    char *format_str="%d PREPARE-STATEMENT\r\nSTATEMENT: %s\r\n\r\n";
    unsigned long buff_size=strlen(format_str)+strlen(request)+2; //add some extra for good measure.
    msg=(char *)malloc(buff_size);
    snprintf(msg,buff_size,format_str,id_cnx,request);
#endif

    cnx->updated_row=-1;

    if(socket_send(cnx,msg) == FOURD_ERROR){
        Free(msg);
        fourd_free_result(res);

        return FOURD_ERROR;
    }

    Free(msg);

    if(receiv_check(cnx,res)!=0)
    {
        fourd_free_result(res);

        return FOURD_ERROR;
    }


    switch(res->resultType)	{
        case UPDATE_COUNT:
            // Get Update-count: Nb row updated
            cnx->updated_row=-1;
            fourd_free_result(res);
            break;
        case RESULT_SET:
            // Get data
            cnx->updated_row=-1;//FIXME
            if(socket_receiv_data(cnx,res) == FOURD_ERROR){
                fourd_free_result(res);

                return FOURD_ERROR;
            }
            break;
        default:
            if ((VERBOSE) || (DEBUG))
                Printferr("Error: Result-Type not supported in query\n");
    }

    fourd_free_result(res);

    return FOURD_OK;
}

/* Sends EXECUTE-STATEMENT msg to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int _query(FOURD *cnx,unsigned int id_cnx,const char *request,FOURD_RESULT *result,const char*image_type, int res_size)
{
    char *msg = NULL;
    char *format_str = NULL;
    size_t buff_size = 0;
    FOURD_RESULT *res = NULL;
    unsigned char *request_b64 = NULL;
    _clear_atr_cnx(cnx);

    /*
    if (DEBUG == 1)
        Printf("Debugging the _query()\n");
    */

    if(_valid_query(cnx,request) == FOURD_ERROR)
        return FOURD_ERROR;

    if(result!= NULL)
        res=result;
    else
        res=calloc(1,sizeof(FOURD_RESULT));

#if STATEMENT_BASE64
    request_b64=b64_encode(request,strlen(request));
	format_str = "%d EXECUTE-STATEMENT\r\nSTATEMENT-BASE64:%s\r\nOutput-Mode:%s\r\n"
              "FIRST-PAGE-SIZE:%i\r\nPREFERRED-IMAGE-TYPES:%s\r\n\r\n";
	buff_size = strlen(format_str) + ndigits(id_cnx) + strlen((const char*)request_b64)
	        + strlen((const char*)OUTPUT_MODE) +  ndigits((unsigned int)res_size) + strlen(image_type);
	msg = (char *)malloc(buff_size);
	snprintf(msg,buff_size,format_str,id_cnx,request_b64,OUTPUT_MODE,res_size,image_type);
	Free(request_b64);
#else
    format_str="%d EXECUTE-STATEMENT\r\nSTATEMENT:%s\r\nOutput-Mode:%s\r\n"
               "FIRST-PAGE-SIZE:%i\r\nPREFERRED-IMAGE-TYPES:%s\r\n\r\n";
    buff_size = strlen(format_str) + ndigits(id_cnx) + strlen(request)
            + strlen((const char*)OUTPUT_MODE) + ndigits((unsigned int)res_size) + strlen(image_type);
    msg = (char *)malloc(buff_size);
    snprintf(msg, buff_size,format_str,id_cnx,request,OUTPUT_MODE,res_size,image_type);
#endif

    cnx->updated_row = -1;

    if(socket_send(cnx,msg) == FOURD_ERROR){
        if (result == NULL)
            fourd_free_result(res);

        Free(msg);
        return FOURD_ERROR;
    }

    Free(msg);

    if(receiv_check(cnx,res) == FOURD_ERROR)
    {
        if (result == NULL)
            fourd_free_result(res);

        return FOURD_ERROR;
    }


    switch(res->resultType)	{
        case UPDATE_COUNT:
            // Get Update-count: Nb row updated
            cnx->updated_row = -1;
            if(socket_receiv_update_count(cnx,res) == FOURD_ERROR){
                _free_data_result(res);

                return FOURD_ERROR;
            }
            _free_data_result(res);
            break;
        case RESULT_SET:
            // Get data
            cnx->updated_row = -1;
            if(socket_receiv_data(cnx,res) == FOURD_ERROR){
                if(result==NULL) {
                    _free_data_result(res);
                }

                return FOURD_ERROR;
            }

            if(result==NULL)
                _free_data_result(res);

            break;
        default:
            if (VERBOSE == 1)
                Printferr("Result-Type not supported");

            if(result==NULL){
                _free_data_result(res);
                cnx->status = FOURD_ERROR;
                cnx->error_code = FOURD_ERROR;
                sprintf_s(cnx->error_string,ERROR_STRING_LENGTH,"Result-Type not supported");
            }else{
                result->status = FOURD_ERROR;
                result->error_code = FOURD_ERROR;
                sprintf_s(result->error_string,ERROR_STRING_LENGTH,"Result-Type not supported");
            }

            return FOURD_ERROR;
    }

    if(result==NULL)
        fourd_free_result(res);
    /*
    if (DEBUG == 1)
        Printf("End of _query()\n");
    */

    return FOURD_OK;
}

/* Sends EXECUTE-STATEMENT msg with bind parameters to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int _query_param(FOURD *cnx,unsigned int id_cnx, const char *request,unsigned int nbParam, const FOURD_ELEMENT *param,
        FOURD_RESULT *result,const char*image_type,int res_size)
{
    char *msg = NULL;
    char *format_str = NULL;
    size_t buff_size = 0;
    FOURD_RESULT *res = NULL;
    char *sParam = NULL;
    unsigned int i = 0;
    char *data = NULL;
    unsigned int data_len = 0;
    unsigned int size = 0;
    size_t paramlen = 0;
    _clear_atr_cnx(cnx);

    /*
    if (DEBUG == 1)
        Printf("Debugging the _query_param()\n");
    */

    if(_valid_query(cnx,request) == FOURD_ERROR)
        return FOURD_ERROR;

    if(nbParam<=0)
        return _query(cnx,id_cnx,request,result,image_type,res_size);

    if(result!=NULL)
        res=result;
    else
        res=calloc(1,sizeof(FOURD_RESULT));

    /* Construct param list */
    // The longest type name is 12 characters, and we add a space between each parameter.
    paramlen=(nbParam+1)*13;

    // Initialized to zero, so we should be able to call strlen() on it without problem
    sParam = calloc(paramlen, sizeof(char));

    //Build the VK string parameters
    for(i=0;i<nbParam;i++)
    {
        snprintf(sParam+strlen(sParam),paramlen-1-strlen(sParam)," %s",stringFromType(param[i].type));

        /* Construct data */
        if(param[i].null==0) {
            data=realloc(data,++size);
            memset(data+(size-1),'1',1);
            data=_serialize(data,&size,param[i].type,param[i].pValue);
        } else {
            if (VERBOSE == 1)
                Printf("Serialize a null value\n");

            data=realloc(data,++size);
            memset(data+(size-1),'0',1);
        }
    }

    data_len=size;

    /* construct Header */
#if STATEMENT_BASE64
	request_b64 = b64_encode(request,strlen(request));
	format_str = "%d EXECUTE-STATEMENT\r\nSTATEMENT-BASE64:%s\r\nOutput-Mode:%s\r\nFIRST-PAGE-SIZE:%i\r\n"
              "PREFERRED-IMAGE-TYPES:%s\r\nPARAMETER-TYPES:%s\r\n\r\n";
	buff_size = strlen(format_str) + ndigits(id_cnx) + strlen(request_b64) + strlen((const char*)OUTPUT_MODE)
            + ndigits((unsigned int)res_size) + strlen(image_type) + strlen((const char*)sParam);
    msg = (char *)malloc(buff_size);
	snprintf(msg,buff_size,format_str,id_cnx,request_b64,OUTPUT_MODE,res_size,image_type,sParam);
	Free(request_b64);
#else
    format_str="%d EXECUTE-STATEMENT\r\nSTATEMENT:%s\r\nOutput-Mode:%s\r\nFIRST-PAGE-SIZE:%i\r\n"
               "PREFERRED-IMAGE-TYPES:%s\r\nPARAMETER-TYPES:%s\r\n\r\n";
    buff_size = strlen(format_str) + ndigits(id_cnx) + strlen(request) + strlen((const char*)OUTPUT_MODE)
            + ndigits((unsigned int)res_size) + strlen(image_type) + strlen((const char*)sParam);
    msg = (char *)malloc(buff_size);
    snprintf(msg,buff_size,format_str,id_cnx,request,OUTPUT_MODE,res_size,image_type,sParam);
#endif

    cnx->updated_row = -1;
    Free(sParam);

    if(socket_send(cnx,msg) == FOURD_ERROR){
        if (result==NULL)
            fourd_free_result(res);

        Free(msg);
        Free(data);
        return FOURD_ERROR;
    }

    Free(msg);

    if(socket_send_data(cnx,data,data_len) == FOURD_ERROR ){
        if (result==NULL)
            fourd_free_result(res);

        Free(data);
        return FOURD_ERROR;
    }

    Free(data);

    if(receiv_check(cnx,res) == FOURD_ERROR)
    {
        if (result==NULL)
            fourd_free_result(res);

        return FOURD_ERROR;
    }


    switch(res->resultType)	{
        case UPDATE_COUNT:
            // Fet Update-count: Nb row updated
            cnx->updated_row = -1;
            if(socket_receiv_update_count(cnx,res) == FOURD_ERROR){
                _free_data_result(res);

                return FOURD_ERROR;
            }
            _free_data_result(res);
            break;
        case RESULT_SET:
            // Get data
            cnx->updated_row = -1;
            if(socket_receiv_data(cnx,res) == FOURD_ERROR){
                if(result==NULL) {
                    _free_data_result(res);
                }

                return FOURD_ERROR;
            }

            if(result==NULL)
                _free_data_result(res);

            break;
        default:
            if (VERBOSE == 1)
                Printferr("Result-Type not supported\n");

            if(result==NULL){
                _free_data_result(res);
                cnx->status = FOURD_ERROR;
                cnx->error_code = FOURD_ERROR;
                sprintf_s(cnx->error_string,ERROR_STRING_LENGTH,"Result-Type not supported");
            }else{
                result->status = FOURD_ERROR;
                result->error_code = FOURD_ERROR;
                sprintf_s(result->error_string,ERROR_STRING_LENGTH,"Result-Type not supported");
            }

            return FOURD_ERROR;
    }

    if(result == NULL)
        fourd_free_result(res);

    if (DEBUG == 1)
        Printf("End of _query_param()\n");

    return FOURD_OK;
}

/* Get next row set in result_set
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int _fetch_result(FOURD_RESULT *res,unsigned int id_cnx)
{
    FOURD *cnx = res->cnx;
    FOURD_RESULT *nRes = NULL;
    void *last_data = NULL;
    unsigned int first_row = res->first_row + res->row_count_sent;
    unsigned int last_row= res->first_row + res->row_count_sent + (PAGE_SIZE-1);
    _clear_atr_cnx(cnx);

    if(last_row>=res->row_count)
        last_row=res->row_count-1;

    nRes=calloc(1,sizeof(FOURD_RESULT));

    // Set parameters unused in socket_receiv
    nRes->first_row = first_row;
    nRes->row_count_sent = last_row-first_row + 1;
    nRes->cnx = res->cnx;
    nRes->row_type = res->row_type;
    nRes->updateability = res->updateability;

    // Get new Result set in new FOURD_RESULT
    cnx->id_cnx++;
    if(__fetch_result(cnx,cnx->id_cnx,res->id_statement,0,first_row,last_row,nRes) == FOURD_ERROR)
    {
        Free(nRes);

        return FOURD_ERROR;
    }


    // Switch data between res and nRes FOURD_RESULT
    last_data=res->elmt;
    res->elmt=nRes->elmt;
    // Important for free memory after
    nRes->elmt=last_data;
    res->first_row=first_row;
    res->row_count_sent=last_row-first_row+1;
    res->error_code=nRes->error_code;
    sprintf_s(res->error_string,sizeof(res->error_string),"%s",nRes->error_string);
    res->status=nRes->status;


    // Free memory
    _free_data_result(nRes);
    Free(nRes);

    return FOURD_OK;
}

/* Low level command
   command_index and statement_id is identify by result of the execute statement command
   Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int __fetch_result(FOURD *cnx,unsigned int id_cnx,int statement_id,int command_index,unsigned int first_row,
        unsigned int last_row,FOURD_RESULT *result)
{
    char *msg = NULL;
    char *format_str = NULL;
    size_t buff_size = 0;
    _clear_atr_cnx(cnx);

    //FIXME?
    if(result == NULL)
        return FOURD_OK;

    format_str = "%d FETCH-RESULT\r\nSTATEMENT-ID:%d\r\nCOMMAND-INDEX:%d\r\nFIRST-ROW-INDEX:%d\r\n"
                 "LAST-ROW-INDEX:%d\r\nOutput-Mode:%s\r\n\r\n",
    buff_size = strlen(format_str) + ndigits(id_cnx) + ndigits((unsigned int)statement_id)
            + ndigits((unsigned int)command_index) + ndigits(first_row) + ndigits(first_row)
            + strlen((const char*)OUTPUT_MODE);
    msg = (char *)malloc(buff_size);
    sprintf_s(msg,buff_size,format_str,id_cnx,statement_id,command_index,first_row,last_row,OUTPUT_MODE);

    if(socket_send(cnx,msg) == FOURD_ERROR){
        Free(msg);

        return FOURD_ERROR;
    }

    Free(msg);

    if(receiv_check(cnx,result) == FOURD_ERROR)
        return FOURD_ERROR;

    if(socket_receiv_data(cnx,result) == FOURD_ERROR)
        return FOURD_ERROR;

    return FOURD_OK;
}

/* Finds a the value of a header section for example
 * msg => Error-Description : Dummy error :> the value is Dummy error
 * get(header,"Error-Description",error_string,ERROR_STRING_LENGTH);
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int get(const char* msg,const char* section,char *value,size_t max_length)
{
    char *loc = NULL;
    char *fin = NULL;
    loc=strstr(msg,section);

    if(loc==NULL)
        return FOURD_ERROR;

    loc+=strlen(section);
    loc=strstr(loc,":");
    if(loc==NULL)
        return FOURD_ERROR;

    loc++;
    fin=strstr(loc,"\n");
    if(fin==NULL)
        return FOURD_ERROR;

    if(*(fin-1)=='\r') {
#ifdef WIN32
        fin--;
#endif
    }

    _snprintf_s(value,max_length,fin-loc,"%s",loc);
    value[fin-loc]=0;

    if(strstr(section,"-Base64") != NULL ) {
        // Decode the value
        unsigned char *value_decode = NULL;
        int len_dec = 0;
        value_decode = b64_decode(value,strlen(value));
        len_dec = (int)strlen((const char *)value_decode);
        value_decode[len_dec]=0;
        strncpy_s(value,max_length,(const char*)value_decode,(size_t)len_dec);
        value[len_dec]=0;
        Free(value_decode);
    }

    return FOURD_OK;
}

/* Replace '] [' by ']\r[' in Column-Aliases-Base64 list header
 * to be splitted by token '\r'*/
void _alias_str_replace(char *list_alias)
{
    char *loc = list_alias;
    char *locm = NULL;

    while((loc=strstr(loc,"] ["))!=NULL) {
        if((loc-list_alias)>1) {
            locm=loc;
            locm--;
            if(locm[0]!=']') {
                loc[1]='\r';
            }
            else {
                loc++;
            }
        }
        else {
            loc[1]='\r';
        }
    }
}

/* Treats the header part of the response after a sending a msg
 * to the 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int treat_header_response(FOURD_RESULT* state)
{
    char *header=state->header;
    FOURD_LONG8 ret_get_status=0;

    // Get status in the header
    state->elmt=0;
    ret_get_status = _get_status(state->header,&(state->status),&(state->error_code),state->error_string);
    if(ret_get_status == FOURD_ERROR)
        return FOURD_ERROR;

    // The header is ok-header

    // get Session-ID
    {
        char column_count[MAX_STRING_NUMBER];
        if(get(header,"Session-ID",column_count,MAX_STRING_NUMBER) == FOURD_OK) {
            state->row_type.nbColumn=(unsigned int) atoi(column_count);
            // Memory allocate for column name and column type
            state->row_type.Column=calloc(state->row_type.nbColumn,sizeof(FOURD_COLUMN));

            /*
            if (DEBUG == 1)
                Printf("Column-Count:%d\n",state->row_type.nbColumn);
            */
        }
    }

    // get Column-Count
    {
        char column_count[MAX_STRING_NUMBER];
        if(get(header,"Column-Count",column_count,MAX_STRING_NUMBER) == FOURD_OK) {
            state->row_type.nbColumn=(unsigned int) atoi(column_count);
            // Memory allocate for column name and column type
            state->row_type.Column=calloc(state->row_type.nbColumn,sizeof(FOURD_COLUMN));

            /*
            if (DEBUG == 1)
                Printf("Column-Count:%d\n",state->row_type.nbColumn);
            */
        }
    }

    // get Column-Types
    {
        char column_type[MAX_COL_TYPES_LENGHT];
        char *column=NULL;
        unsigned int num=0;
        if(get(header,"Column-Types",column_type,MAX_COL_TYPES_LENGHT) == FOURD_OK) {

            /*
            if (DEBUG == 1)
                Printf("Column-Types => '%s'\n",column_type);
            */

            column = strtok_s(column_type, " ",&context);
            if(column!=NULL)
                do{

                    /*
                    if (DEBUG == 1)
                        Printf("Column %d: %s (%s)\n",num+1,column,stringFromType(typeFromString(column)));
                    */

                    if(num<state->row_type.nbColumn) {
                        state->row_type.Column[num].type=typeFromString(column);
                        strncpy_s(state->row_type.Column[num].sType,MAX_COL_TYPES_LENGHT,column,strlen(column)+1);
                    }
                    else {
                        if (VERBOSE == 1)
                            Printferr("There is more columns than Column-Count\n");
                    }
                    num++;
                    column = strtok_s(NULL, " ",&context);
                }while(column!=NULL);

                /*
            if (DEBUG == 1)
                Printf("End of reading columns\n");
            */
        }
    }


    // get Column-Aliases-Base64
    {
        char *column_alias;char *alias=NULL;
        unsigned int num=0;
        char * col_start;
        char * col_fin;
        size_t base64_size=MAX_COL_TYPES_LENGHT;
        char *section="Column-Aliases-Base64";

        //Figure out the length of our section.
        //Start by getting a pointer to the start of the section label
        col_start=strstr(header,section);
        if(col_start!=NULL){
            //advance the pointer by the length of the section label
            col_start+=strlen(section);
            //and find the first : (probably the next character)
            col_start=strstr(col_start,":");

            if(col_start!=NULL){
                //after making sure we still have something to work with,
                //advance to the next character after the ":", which is the
                //start of our data
                col_start++;

                //now find the end. It should have a new line after it
                col_fin=strstr(col_start,"\n");
                if(col_fin!=NULL){
                    //we have pointers to the start and end of our data. So how long is it?
                    //just subtract the pointers!
                    base64_size=col_fin-col_start;
                }
            }
        }
        //if we ran into any issues with the above manipulation, we just use the
        //default size

        column_alias=calloc(sizeof(char), base64_size); //I always like to give a few bytes wiggle

        if(get(header,"Column-Aliases-Base64",column_alias,base64_size) == FOURD_OK) {
            /* delete the last space char if exist */
            if(column_alias[strlen(column_alias)-1]==' ')
                column_alias[strlen(column_alias)-1]=0;

            if (DEBUG == 1)
                Printf("Column-Aliases-Base64 => '%s'\n\n",column_alias);

            _alias_str_replace(column_alias);
            alias = strtok_s(column_alias, "\r",&context);
            if(alias!=NULL)
                do{
                    /*
                    if (DEBUG == 1)
                        Printf("Alias %d: '%s'\n",num+1,alias);
                        */

                    if(num<state->row_type.nbColumn) {
                        /* erase [] */
                        if(*alias=='[' && alias[strlen(alias)-1]==']') {
                            strncpy_s(state->row_type.Column[num].sColumnName,MAX_COL_TYPES_LENGHT,alias+1,strlen(alias)-2);
                        } else {
                            strncpy_s(state->row_type.Column[num].sColumnName,MAX_COL_TYPES_LENGHT,alias,strlen(alias));
                        }
                    }else {
                        if (VERBOSE == 1)
                            Printferr("There is more alias than Column-Count\n");

                        free(column_alias);

                        return FOURD_ERROR;
                    }
                    num++;
                    alias = strtok_s(NULL, "\r",&context);
                }while(alias!=NULL);
            /*
            if (DEBUG == 1)
                Printf("End reading alias\n");
            */
        }

        free(column_alias);
    }

    // get Row-Count
    {
        char row_count[MAX_STRING_NUMBER];
        if(get(header,"Row-Count",row_count,MAX_STRING_NUMBER) == FOURD_OK) {
            state->row_count=(unsigned int) atoi(row_count);

            /*
            if (DEBUG == 1)
                Printf("Row-Count:%d\n",state->row_count);
            */

        }
    }

    // get Row-Count-Sent
    {
        char row_count[MAX_STRING_NUMBER];
        if(get(header,"Row-Count-Sent",row_count,MAX_STRING_NUMBER) == FOURD_OK) {

            /*
            if (DEBUG == 1)
                Printf("Row-Count-Sent:\"%s\" <=lut\n",row_count);
            */

            state->row_count_sent=(unsigned int) atoi(row_count);

            /*
            if (DEBUG == 1)
                Printf("Row-Count-Sent:%d\n",state->row_count_sent);
            */
        }
    }


    // get Statement-ID
    {
        char statement_id[MAX_STRING_NUMBER];
        if(get(header,"Statement-ID",statement_id,MAX_STRING_NUMBER) == FOURD_OK) {
            state->id_statement=atoi(statement_id);

            /*
            if (DEBUG == 1)
                Printf("Statement-ID:%d\n",state->id_statement);
            */

        }
    }

    // Column-Updateability
    {
        char updateability[MAX_COL_TYPES_LENGHT];

        if(get(header,"Column-Updateability",updateability,MAX_COL_TYPES_LENGHT) == FOURD_OK) {
            state->updateability=(strstr(updateability,"Y")!=NULL);

            /*
            if (DEBUG == 1)
            {
                Printf("Column-Updateability:%s\n",updateability);
                Printf("Column-Updateability:%d\n",state->updateability);
            }*/

        }
    }

    // get Result-Type
    {
        char result_type[MAX_COL_TYPES_LENGHT];
        if(get(header,"Result-Type",result_type,MAX_COL_TYPES_LENGHT) == FOURD_OK) {
            str_strip(result_type);

            //if Result-Type contains more than 1 Result-type => multiquery => not supported by this driver
            if(strstr(result_type," ")!=NULL)
            {
                // Multiquery not supported by this driver

                if (DEBUG == 1)
                {
                    Printf("Result-Type:'%s'\n",result_type);
                    Printf("Position %d\n",strstr(result_type," ")-result_type);
                    Printferr("Error: Multiquery not supported\n");
                }

                return FOURD_ERROR;
            }

            state->resultType=resultTypeFromString(result_type);
            switch(state->resultType) {
                case UPDATE_COUNT:
                    break;
                case RESULT_SET:
                    break;
                default:
                    if (VERBOSE == 1)
                        Printferr("Result-Type not supported");

                    return FOURD_ERROR;
            }

        }
    }

    return FOURD_OK;
}

/* Get the response status OK or ERROR?
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
FOURD_LONG8 _get_status(const char *header,int *status, FOURD_LONG8 *error_code,char *error_string)
{
    char *loc=NULL,*fin=NULL,sStatus[50];
    *status=FOURD_ERROR;
    loc=strstr(header," ");
    if(loc==NULL)
        return FOURD_ERROR;

    loc++;
    fin=strstr(loc,"\n");
    if(fin==NULL)
        return FOURD_ERROR;

    if(*(fin-1)=='\r') {
#ifdef WIN32
        fin--;
#endif
    }
    _snprintf_s(sStatus,50,fin-loc,"%s",loc);
    status[fin-loc]=0;
    if(strcmp(sStatus,"OK")==0) {
        // It's ok
        *error_code=0;
        error_string[0]=0;
        *status=FOURD_OK;
        return FOURD_OK;
    }
    else {
        // There is an error
        *status=FOURD_ERROR;
        {
            char error[50];
            get(header,"Error-Code",error,50);
            *error_code=atoi(error);
        }
        get(header,"Error-Description",error_string,ERROR_STRING_LENGTH);
        return *error_code;
    }

    return FOURD_ERROR;
}

/* Check the response
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int receiv_check(FOURD *cnx,FOURD_RESULT *state)
{

    if(socket_receiv_header(cnx,state) == FOURD_ERROR){
        if (VERBOSE == 1)
            Printferr("Error in socket_receiv_header\n");

        return FOURD_ERROR;
    }


    if(treat_header_response(state) == FOURD_ERROR) {
        if (VERBOSE == 1)
            Printferr("Error in treat_header_response\n");

        cnx->status=state->status;
        cnx->error_code=state->error_code;
        _snprintf(cnx->error_string,ERROR_STRING_LENGTH,"%s",state->error_string);
        return FOURD_ERROR;
    }

    cnx->status=state->status;
    cnx->error_code=state->error_code;
    strncpy_s(cnx->error_string,ERROR_STRING_LENGTH,state->error_string,ERROR_STRING_LENGTH);
    return FOURD_OK;
}

/* Sends CLOSE-STATEMENT msg to 4D server
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int close_statement(FOURD_RESULT *res,unsigned int id_cnx)
{
    char *msg = NULL;
    char *format_str = NULL;
    size_t buff_size = 0;
    FOURD *cnx=NULL;
    FOURD_RESULT state;
    _clear_atr_cnx(cnx);

    if(res==NULL)
        return 0;

    cnx=res->cnx;

    format_str = "%d CLOSE-STATEMENT\r\nSTATEMENT-ID:%d\r\n\r\n";
    buff_size = strlen(format_str) + ndigits(id_cnx) + ndigits((unsigned int)res->id_statement);
    msg = (char *)malloc(buff_size);
    sprintf_s(msg,buff_size,format_str,id_cnx,res->id_statement);

    if(socket_send(cnx,msg) == FOURD_ERROR)
    {
        Free(msg);

        return FOURD_ERROR;
    }

    Free(msg);

    if(receiv_check(cnx,&state) == FOURD_ERROR)
        return FOURD_ERROR;

    return FOURD_OK;
}

/* Frees memory allocated by FOURD_RESULT struct */
void _free_data_result(FOURD_RESULT *res)
{

    unsigned int nbCol=res->row_type.nbColumn;
    unsigned int nbRow=res->row_count_sent;
    unsigned int nbElmt=nbCol*nbRow;
    unsigned int i=0;
    FOURD_ELEMENT *pElmt=res->elmt;
    if(pElmt==NULL) {
        return;
    }
    for(i=0;i<nbElmt;i++,pElmt++)
    {
        switch(pElmt->type) {
            case VK_BOOLEAN:
            case VK_BYTE:
            case VK_WORD:
            case VK_LONG:
            case VK_LONG8:
            case VK_REAL:
            case VK_DURATION:
            case VK_TIMESTAMP:
                Free(pElmt->pValue);
                break;
            case VK_FLOAT:
                FreeFloat((FOURD_FLOAT *)pElmt->pValue);
                break;
            case VK_STRING:
                FreeString((FOURD_STRING *)pElmt->pValue);
                break;
            case VK_BLOB:
                FreeBlob((FOURD_BLOB *)pElmt->pValue);
                break;
            case VK_IMAGE:
                FreeImage((FOURD_IMAGE *)pElmt->pValue);
                break;
            default:
                break;
        }
    }

    Free(res->elmt);
}

/* Frees memory allocated by FOURD_STATEMENT struct */
void _free_fourd_statement_elmts(FOURD_STATEMENT *state)
{
    unsigned int nbElmt=state->nbAllocElement;
    unsigned int i=0;
    FOURD_ELEMENT *pElmt=state->elmt;
    if(pElmt==NULL) {
        return;
    }
    for(i=0;i<nbElmt;i++,pElmt++)
    {
        switch(pElmt->type) {
            case VK_BOOLEAN:
            case VK_BYTE:
            case VK_WORD:
            case VK_LONG:
            case VK_LONG8:
            case VK_REAL:
            case VK_DURATION:
            case VK_TIMESTAMP:
                Free(pElmt->pValue);
                break;
            case VK_FLOAT:
                FreeFloat((FOURD_FLOAT *)pElmt->pValue);
                break;
            case VK_STRING:
                FreeString((FOURD_STRING *)pElmt->pValue);
                break;
            case VK_BLOB:
                FreeBlob((FOURD_BLOB *)pElmt->pValue);
                break;
            case VK_IMAGE:
                FreeImage((FOURD_IMAGE *)pElmt->pValue);
                break;
            default:
                break;
        }
    }

    Free(state->elmt);
}

/* Memory Allocation */
void *_copy(FOURD_TYPE type,void *org)
{
    void *buff=NULL;
    //int size=0;
    if(org!=NULL)
    {
        switch(type) {
            case VK_BOOLEAN:
            case VK_BYTE:
            case VK_WORD:
            case VK_LONG:
                if (VERBOSE || DEBUG)
                    Printf("*******Bind %d ********\n",*(FOURD_LONG*)org);
            case VK_LONG8:
            case VK_REAL:
            case VK_DURATION:
            case VK_TIMESTAMP:
                buff=calloc(1,(size_t)vk_sizeof(type));
                memcpy(buff,org,vk_sizeof(type));
                break;
            case VK_FLOAT:
            {
                FOURD_FLOAT *f=org;
                FOURD_FLOAT *cp=NULL;
                cp=calloc(1,sizeof(FOURD_FLOAT));
                cp->data=calloc(1,(size_t)f->data_length);
                cp->exp=f->exp;
                cp->sign=f->sign;
                cp->data_length=f->data_length;
                memcpy(cp->data,f->data,f->data_length);
                buff=cp;
            }
                break;
            case VK_STRING:
            {
                FOURD_STRING *src=org;
                FOURD_STRING *cp=NULL;
                cp=calloc(1,sizeof(FOURD_STRING));
                cp->data=calloc((size_t)src->length,2);	/* 2 bytes per char */
                cp->length=src->length;
                memcpy(cp->data,src->data,src->length*2);  /* 2 bytes per char */
                buff=cp;
            }
                break;
            case VK_BLOB:
            {
                FOURD_BLOB *src=org;
                FOURD_BLOB *cp=NULL;
                cp=calloc(1,sizeof(FOURD_BLOB));
                cp->data=calloc((size_t)src->length,1);
                cp->length=src->length;
                memcpy(cp->data,src->data,src->length);
                buff=cp;
            }
                break;
            case VK_IMAGE:
            {
                FOURD_IMAGE *src=org;
                FOURD_IMAGE *cp=NULL;
                cp=calloc(1,sizeof(FOURD_IMAGE));
                cp->data=calloc((size_t)src->length,1);
                cp->length=src->length;
                memcpy(cp->data,src->data,src->length);
                buff=cp;
                break;
            }
                break;
            default:
                break;
        }
    }
    return buff;
}

char *_serialize(char *data,unsigned int *size, FOURD_TYPE type, void *pObj)
{
    int lSize=0;
    if(pObj!=NULL) {
        switch(type) {
            case VK_BOOLEAN:
            case VK_BYTE:
            case VK_WORD:
            case VK_LONG:
                if (VERBOSE || DEBUG)
                    Printf("*******Serialize %d ********\n",*(FOURD_LONG*)pObj);
            case VK_LONG8:
            case VK_REAL:
            case VK_DURATION:
                lSize=vk_sizeof(type);
                data=realloc(data,(*size)+lSize);
                memcpy(data+*size,pObj,lSize);
                *size+=lSize;
                break;
            case VK_TIMESTAMP:/* Use other procedure for serialize this one because structure can align */
            {
                FOURD_TIMESTAMP *o=pObj;
                lSize=sizeof(o->year)+sizeof(o->mounth)+sizeof(o->day)+sizeof(o->milli);
                data=realloc(data,(*size)+lSize);
                memcpy(data+*size,&(o->year),2);
                memcpy(data+*size+2,&(o->year),1);
                memcpy(data+*size+3,&(o->year),1);
                memcpy(data+*size+4,&(o->year),4);
                *size+=lSize;
            }
                break;
            case VK_FLOAT:
            {
                FOURD_FLOAT *o=pObj;
                lSize=sizeof(o->exp)+sizeof(o->sign)+sizeof(o->data_length)+o->data_length;
                data=realloc(data,(*size)+lSize);
                memcpy(data+*size,&(o->exp),4);
                memcpy(data+*size+4,&(o->sign),1);
                memcpy(data+*size+5,&(o->data_length),4);
                memcpy(data+*size+9,o->data,o->data_length);
                *size+=lSize;
            }
                break;
            case VK_STRING:
            {
                FOURD_STRING *o=pObj;
                int len=o->length;
                len=-len;
                lSize=sizeof(o->length)+o->length*2;
                data=realloc(data,(*size)+lSize);
                memcpy(data+*size,&len,4);
                memcpy(data+*size+4,o->data,o->length*2);
                *size+=lSize;
            }
                break;
            case VK_BLOB:
            {
                FOURD_BLOB *o=pObj;
                lSize=sizeof(o->length)+o->length*2;
                data=realloc(data,(*size)+lSize);
                memcpy(data+*size,&(o->length),4);
                memcpy(data+*size+4,o->data,o->length*2);
                *size+=lSize;
            }
                break;
            case VK_IMAGE:
            {
                FOURD_IMAGE *o=pObj;
                lSize=sizeof(o->length)+o->length*2;
                data=realloc(data,(*size)+lSize);
                memcpy(data+*size,&(o->length),4);
                memcpy(data+*size+4,o->data,o->length*2);
                *size+=lSize;
            }
                break;
            default:
                break;
        }
    }
    return data;
}

void Free(void *p)
{
    if(p)
    {
        free(p);
        p=NULL;
    }
}

void FreeFloat(FOURD_FLOAT *p)
{
    if(p) {
        Free(p->data);
        Free(p);
    }
}

void FreeString(FOURD_STRING *p)
{
    if(p) {
        Free(p->data);
        Free(p);
    }
}

void FreeBlob(FOURD_BLOB *p)
{
    if(p) {
        Free(p->data);
        Free(p);
    }
}

void FreeImage(FOURD_IMAGE *p)
{
    if(p) {
        Free(p->data);
        Free(p);
    }
}

void PrintData(const void *data,size_t size)
{
    const char *d=data;
    unsigned int i=0;
    if(size>=1)
            Printf("0x%X",*(char *)(d+i));

    for(i=1;i<size;i++)
        Printf(" 0x%X",*(char *)(d+i));

}
