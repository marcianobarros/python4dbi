#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fourd.h"
#include "fourd_int.h"
#include "utils.h"
#include "communication.h"

/* Returns the FOURD_TYPE enum from a str */
FOURD_TYPE typeFromString(const char *type)
{
	if(strcmp(type,"VK_BOOLEAN")==0)
		return VK_BOOLEAN;
	if(strcmp(type,"VK_BYTE")==0)
		return VK_BYTE;
	if(strcmp(type,"VK_WORD")==0)
		return VK_WORD;
	if(strcmp(type,"VK_LONG")==0)
		return VK_LONG;
	if(strcmp(type,"VK_LONG8")==0)
		return VK_LONG8;
	if(strcmp(type,"VK_REAL")==0)
		return VK_REAL;
	if(strcmp(type,"VK_FLOAT")==0)
		return VK_FLOAT;
	if(strcmp(type,"VK_TIMESTAMP")==0)
		return VK_TIMESTAMP;
	if(strcmp(type,"VK_TIME")==0)
		return VK_TIMESTAMP;
	if(strcmp(type,"VK_DURATION")==0)
		return VK_DURATION;
	if(strcmp(type,"VK_TEXT")==0)
		return VK_STRING;
	if(strcmp(type,"VK_STRING")==0)
		return VK_STRING;
	if(strcmp(type,"VK_BLOB")==0)
		return VK_BLOB;
	if(strcmp(type,"VK_IMAGE")==0)
		return VK_IMAGE;

	return VK_UNKNOW;
}

/* Returns the FOURD_TYPE str from an enum */
const char* stringFromType(FOURD_TYPE type)
{
	switch(type)
	{
		case VK_BOOLEAN:
			return "VK_BOOLEAN";
		case VK_BYTE:
			return "VK_BYTE";
		case VK_WORD:
			return "VK_WORD";
		case VK_LONG:
			return "VK_LONG";
		case VK_LONG8:
			return "VK_LONG8";
		case VK_REAL:
			return "VK_REAL";
		case VK_FLOAT:
			return "VK_FLOAT";
		case VK_TIMESTAMP:
			return "VK_TIMESTAMP";
		case VK_TIME:
			return "VK_TIME";
		case VK_DURATION:
			return "VK_DURATION";
		case VK_STRING:
			return "VK_STRING";
		case VK_BLOB:
			return "VK_BLOB";
		case VK_IMAGE:
			return "VK_IMAGE";
		default:
			return "VK_UNKNOW";
	}
}

/* Return sizeof FOURD_TYPE or -1 if variable length or 0 if unknown type */
int vk_sizeof(FOURD_TYPE type)
{
	switch(type)
	{
		case VK_BOOLEAN:
		case VK_BYTE:
		case VK_WORD:
			return 2;
		case VK_LONG:
			return 4;
		case VK_LONG8:
		case VK_REAL:
		case VK_DURATION:
			return 8;
		case VK_FLOAT:
			return -1;
		case VK_TIME:
		case VK_TIMESTAMP:
			return 8;
		case VK_TEXT:
		case VK_STRING:
		case VK_BLOB:
		case VK_IMAGE:
			return -1;
		default:
			if (VERBOSE == 1)
				Printferr("Error: Unknown type in vk_sizeof function\n");
			return 0;
	}
}

/* Returns the FOURD_RESULT_TYPE enum from a str */
FOURD_RESULT_TYPE resultTypeFromString(const char *type)
{
	if(strcmp(type,"Update-Count")==0)
		return UPDATE_COUNT;
	if(strcmp(type,"Result-Set")==0)
		return RESULT_SET;
	return UNKNOW;
}

/* Returns the FOURD_RESULT_TYPE str from an enum */
const char* stringFromResultType(FOURD_RESULT_TYPE type)
{
	switch(type)
	{
		case UPDATE_COUNT:
			return "Update-Count";
		case RESULT_SET:
			return "Result-Set";
		default:
			return "Unknown";
	}
}

/* Sets the cnx PREFERRED-IMAGE-TYPES */
void fourd_set_preferred_image_types(FOURD* cnx,const char *types)
{
	if(cnx->preferred_image_types)
		Free(cnx->preferred_image_types);

	if(types){
		cnx->preferred_image_types=malloc(strlen(types)+1);
		sprintf_s(cnx->preferred_image_types,strlen(types)+1,"%s",types);
	}
	else{
		sprintf_s(cnx->preferred_image_types,strlen("jpg")+1,"%s","jpg");
	}

}

/* Returns a FOURD connection struct initialized */
FOURD* fourd_init()
{
	FOURD* cnx=calloc(1,sizeof(FOURD));

#ifdef WIN32
	int iResult=0;
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &cnx->wsaData);
    if (iResult != 0) {
        if (VERBOSE || DEBUG)
            Printferr("WSAStartup failed: %d\n", iResult);

        Free(cnx);
        return NULL;
    }
#endif /* WIN32 */

	cnx->socket = INVALID_SOCKET;
	cnx->status = FOURD_OK;
	cnx->init = 1;
	fourd_set_preferred_image_types(cnx,DEFAULT_IMAGE_TYPE);

	return cnx;
}

/* Connects the cnx to 4D Server with LOGIN
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int fourd_connect(FOURD *cnx,const char *host,const char *user,const char *password,const char *base,unsigned int port)
{
	if(!cnx->init){
		if (VERBOSE == 1)
			Printferr("FOURD object did not initialised\n");

		cnx->status=FOURD_ERROR;
		cnx->error_code=FOURD_ERROR;
		strncpy_s(cnx->error_string,ERROR_STRING_LENGTH,"FOURD object did not initialised",ERROR_STRING_LENGTH);

		return FOURD_ERROR;
	}

	if(cnx->connected){
		if (VERBOSE == 1)
			Printferr("Already connected to 4D server\n");

        cnx->status=FOURD_ERROR;
        cnx->error_code=FOURD_ERROR;
		strncpy_s(cnx->error_string,ERROR_STRING_LENGTH,"Already connected to 4D server",ERROR_STRING_LENGTH);
		return FOURD_ERROR;
	}

	if(socket_connect_timeout(cnx,host,port,SOCKET_TIMEOUT) == FOURD_ERROR){
		if (VERBOSE == 1)
			Printferr("Error in socket_connect\n");

		cnx->connected=0;
		if(cnx->error_code==0) {
			cnx->status = FOURD_ERROR;
			cnx->error_code = FOURD_ERROR;
			strncpy_s(cnx->error_string, ERROR_STRING_LENGTH, "Error during connection to 4D server",
					  ERROR_STRING_LENGTH);
		}

		return FOURD_ERROR;
	}

	/* Assume that is connected beacause of is connected validation in socket_send */
	cnx->connected = 1;
	if(dblogin(cnx,1,user,((password==NULL)?"":password),cnx->preferred_image_types) == FOURD_ERROR){
		if (VERBOSE == 1)
			Printferr("Error during login to 4D server\n");

		cnx->connected=0;
		if(cnx->error_code==0){
			cnx->status = FOURD_ERROR;
			cnx->error_code = FOURD_ERROR;
			strncpy_s(cnx->error_string,ERROR_STRING_LENGTH,"Error during login to 4D server",ERROR_STRING_LENGTH);
		}

		return FOURD_ERROR;
	}

	cnx->connected=1;
	cnx->error_code=0;
	strncpy_s(cnx->error_string,ERROR_STRING_LENGTH,"",ERROR_STRING_LENGTH);

	return FOURD_OK;
}

/* Disconnects the cnx to 4D Server with LOGOUT
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int fourd_close(FOURD *cnx)
{

	if(!cnx->connected){
		if (VERBOSE == 1)
			Printferr("Not connected to 4D Server\n");

		cnx->status=FOURD_ERROR;
		cnx->error_code=FOURD_ERROR;
		strncpy_s(cnx->error_string,ERROR_STRING_LENGTH,"Not connected to 4D Server",ERROR_STRING_LENGTH);

		return FOURD_ERROR;
	}

	cnx->id_cnx++;
	if(dblogout(cnx,cnx->id_cnx) == FOURD_ERROR){
		if (VERBOSE == 1)
			Printferr("Error during logout from 4D server\n");

		socket_disconnect(cnx);

		if(cnx->error_code==0){
			cnx->status = FOURD_ERROR;
			cnx->error_code = FOURD_ERROR;
			strncpy_s(cnx->error_string,ERROR_STRING_LENGTH,"Error during logout from 4D server",ERROR_STRING_LENGTH);
		}

		return FOURD_ERROR;
	}

	cnx->id_cnx++;
	if(quit(cnx,cnx->id_cnx) == FOURD_ERROR){
		if (VERBOSE == 1)
			Printferr("Error during quit from 4D server\n");

		socket_disconnect(cnx);

		if(cnx->error_code==0){
			cnx->status = FOURD_ERROR;
			cnx->error_code = FOURD_ERROR;
			strncpy_s(cnx->error_string,ERROR_STRING_LENGTH,"Error during quit from 4D server",ERROR_STRING_LENGTH);
		}

		return FOURD_ERROR;
	}

	socket_disconnect(cnx);

	return FOURD_OK;
}

/* Frees the cnx struct */
void fourd_free(FOURD* cnx)
{
#ifdef WIN32
	WSACleanup();
#endif

	Free(cnx->preferred_image_types);
	Free(cnx);
}

/* Executes a query command no data is return
 * The default PAGE_SIZE is used
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int fourd_exec(FOURD *cnx,const char *query)
{
	cnx->id_cnx++;
	return _query(cnx,cnx->id_cnx,query,NULL,cnx->preferred_image_types,PAGE_SIZE);
}

/* Get the connection number of affected rows */
FOURD_LONG8 fourd_affected_rows(FOURD *cnx)
{
	return cnx->updated_row;
}

/* Get the connection error number */
int fourd_errno(FOURD *cnx)
{
	return (int)cnx->error_code;
}

/* Get the connection error string */
const char * fourd_error(FOURD *cnx)
{
	return cnx->error_string;
}

/* Get the total number of rows of the result query */
FOURD_LONG8 fourd_num_rows(FOURD_RESULT *result)
{
    return result->row_count;
}

/* Executes a query command returns
 * the result data struct
 * The default PAGE_SIZE is used */
FOURD_RESULT* fourd_query(FOURD *cnx,const char *query)
{
	FOURD_RESULT* result;

	result=calloc(1,sizeof(FOURD_RESULT));
	result->cnx=cnx;
	cnx->id_cnx++;
	if(_query(cnx,cnx->id_cnx,query,result,cnx->preferred_image_types,PAGE_SIZE) == FOURD_OK){
		result->numRow = -1; /*current row index*/
		return result;
	}
	else
	{
		fourd_free_result(result);
		return NULL;
	}
}

/* Executes a query command returns
 * the result data struct
 * The PAGE_SIZE can be set */
FOURD_RESULT *fourd_exec_statement(FOURD_STATEMENT *state, int res_size)
{
	FOURD_RESULT *result=NULL;

	result=calloc(1,sizeof(FOURD_RESULT));
	result->cnx=state->cnx;
	state->cnx->id_cnx++;

	if(_query_param(state->cnx,state->cnx->id_cnx,state->query,state->nb_element,state->elmt,result,state->preferred_image_types,res_size)==0)
	{
		result->numRow=-1; /*current row index*/
		return result;
	}
	else
	{
		fourd_free_result(result);
		return NULL;
	}
}

/* Clears FOURD_RESULT */
void fourd_free_result(FOURD_RESULT *res)
{
	if(res!=NULL)
	{
		if(res->elmt!=NULL)
			_free_data_result(res);

		if(res->header!=NULL)
			Free(res->header);

		if(res->row_type.Column!=NULL)
			Free(res->row_type.Column);

		Free(res);
	}
}

/* Prepares and returns the FOURD_STATEMENT */
FOURD_STATEMENT * fourd_prepare_statement(FOURD *cnx,const char *query)
{
	FOURD_STATEMENT* state=NULL;

	if(cnx==NULL || !cnx->connected || query==NULL)
		return NULL;

	cnx->id_cnx++;
	_prepare_statement(cnx, cnx->id_cnx, query);

	state=calloc(1,sizeof(FOURD_STATEMENT));
	state->cnx=cnx;
	state->query=(char *)malloc(strlen(query)+1);

	// Allocate arbitrarily five elements in this table
	// state->nbAllocElement=5;
	// state->elmt=calloc(state->nbAllocElement,sizeof(FOURD_ELEMENT));
	state->nbAllocElement=0;
	state->nb_element=0;

	// copy query into statement
	sprintf(state->query,"%s",query);
	fourd_set_statement_preferred_image_types(state,cnx->preferred_image_types);

	return state;
}

/* Clears FOURD_STATEMENT */
void fourd_free_statement(FOURD_STATEMENT *state){

	if (state->query!=NULL){
		free(state->query);
		state->query=NULL;
	}

	if(state->elmt!=NULL){
		_free_fourd_statement_elmts(state);
		state->elmt=NULL;
	}

	if (state->preferred_image_types!=NULL){
		free(state->preferred_image_types);
		state->preferred_image_types=NULL;
	}

	Free(state);
}

/* Closes the STATEMENT
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int fourd_close_statement(FOURD_RESULT *res)
{
	res->cnx->id_cnx++;
	if(close_statement(res,res->cnx->id_cnx) == FOURD_ERROR)
		return FOURD_ERROR;

	return FOURD_OK;
}

/* Closes the next result page
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int fourd_next_row(FOURD_RESULT *res)
{
	res->numRow++;
	if(res->numRow>=res->row_count)
		return FOURD_ERROR;

	if(res->numRow > res->first_row+res->row_count_sent-1) {
		res->cnx->id_cnx++;
		if(_fetch_result(res,res->cnx->id_cnx) == FOURD_ERROR)
			return FOURD_ERROR;
	}

	return FOURD_OK;
}

/* Sets the connection timeout */
void fourd_timeout(FOURD* cnx,int timeout)
{
	cnx->timeout=timeout;
}

/* Returns field casted to FOURD_LONG */
FOURD_LONG * fourd_field_long(FOURD_RESULT *res,unsigned int numCol)
{
	unsigned int nbCol=res->row_type.nbColumn;
	unsigned int nbRow=res->row_count;
	FOURD_ELEMENT *elmt;
	unsigned int indexElmt=0;	/* index of element in table <> numRow*nbCol + numCol */

	if(res->numRow>=nbRow){
		res->cnx->status=FOURD_ERROR;
		res->cnx->error_code=FOURD_ERROR;
		sprintf_s(res->cnx->error_string,ERROR_STRING_LENGTH,"Number of row out of bounds",ERROR_STRING_LENGTH);
		return 0;
	}

	if(numCol>=nbCol){
		res->cnx->status=FOURD_ERROR;
		res->cnx->error_code=FOURD_ERROR;
		sprintf_s(res->cnx->error_string,ERROR_STRING_LENGTH,"Number column out of bounds",ERROR_STRING_LENGTH);
		return 0;
	}

	indexElmt=(res->numRow-res->first_row)*nbCol+numCol;
	elmt=&(res->elmt[indexElmt]);
	if(elmt->null==0)
		return (FOURD_LONG *)elmt->pValue;


	return 0;
}

/* Returns field casted to FOURD_STRING */
FOURD_STRING * fourd_field_string(FOURD_RESULT *res,unsigned int numCol)
{
	int nbCol=res->row_type.nbColumn;
	int nbRow=res->row_count;
	unsigned int indexElmt=0;	/*index of element in table <> numRow*nbCol + numCol */
	FOURD_ELEMENT *elmt=NULL;

	if(res->numRow>=nbRow){
		res->cnx->status=FOURD_ERROR;
		res->cnx->error_code=FOURD_ERROR;
		sprintf_s(res->cnx->error_string,ERROR_STRING_LENGTH,"Number of row out of bounds",ERROR_STRING_LENGTH);
		return NULL;
	}

	if(numCol>=nbCol){
		res->cnx->status=FOURD_ERROR;
		res->cnx->error_code=FOURD_ERROR;
		sprintf_s(res->cnx->error_string,ERROR_STRING_LENGTH,"Number of column out of bounds",ERROR_STRING_LENGTH);
		return NULL;
	}

	indexElmt=(res->numRow-res->first_row)*nbCol+numCol;
	elmt=&(res->elmt[indexElmt]);
	if(elmt->null==0)
	{
		FOURD_STRING *x=(FOURD_STRING *)elmt->pValue;
		return x;
	}

	return NULL;
}

/* Returns field */
void * fourd_field(FOURD_RESULT *res,unsigned int numCol)
{
	unsigned int nbCol=res->row_type.nbColumn;
	unsigned int nbRow=res->row_count;
	unsigned int indexElmt=0;	/* index of element in table <> numRow*nbCol + numCol */
	FOURD_ELEMENT *elmt=NULL;

	if(res->numRow>=nbRow){
		res->cnx->status=FOURD_ERROR;
		res->cnx->error_code=FOURD_ERROR;
		sprintf_s(res->cnx->error_string,ERROR_STRING_LENGTH,"Number of row out of bounds",ERROR_STRING_LENGTH);
		return NULL;
	}

	if(numCol>=nbCol){
		res->cnx->status=FOURD_ERROR;
		res->cnx->error_code=FOURD_ERROR;
		sprintf_s(res->cnx->error_string,ERROR_STRING_LENGTH,"Number of column out of bounds",ERROR_STRING_LENGTH);
		return NULL;
	}

	indexElmt=(res->numRow-res->first_row)*nbCol+numCol;

	elmt=&(res->elmt[indexElmt]);
	if(elmt->null!=0)
		return NULL;

	return elmt->pValue;
}

//FIXME
/* Converts a fourd element value to a string
 * Returns 0=> OK (FOURD_OK) , 1=> KO  (FOURD_ERROR) */
int fourd_field_to_string(FOURD_RESULT *res,unsigned int numCol,char **value,size_t *len)
{
	unsigned int nbCol=res->row_type.nbColumn;
	unsigned int nbRow=res->row_count;
	FOURD_ELEMENT *elmt=NULL;
	unsigned int indexElmt=0;	/* index of element in table <> numRow*nbCol + numCol */

	if(res->numRow>=nbRow){
		*value=NULL;
		*len=0;
		res->cnx->status=FOURD_ERROR;
		res->cnx->error_code=FOURD_ERROR;
		sprintf_s(res->cnx->error_string,ERROR_STRING_LENGTH,"Number of row out of bounds",ERROR_STRING_LENGTH);
		return FOURD_ERROR;
	}

	if(numCol>=nbCol){
		*value=NULL;
		*len=0;
		res->cnx->status=FOURD_ERROR;
		res->cnx->error_code=FOURD_ERROR;
		sprintf_s(res->cnx->error_string,ERROR_STRING_LENGTH,"Number of column out of bounds",ERROR_STRING_LENGTH);
		return FOURD_ERROR;
	}

	indexElmt=(res->numRow-res->first_row)*nbCol+numCol;
	elmt=&(res->elmt[indexElmt]);
	if(elmt->null!=0) {	/*if elmt is null*/
		*value=NULL;
		*len=0;
	}
	else {
		switch(elmt->type) {
			case VK_BOOLEAN:
			{
				*value=calloc(2,sizeof(char));
				sprintf_s(*value,2,"%s",(*((FOURD_BOOLEAN *)elmt->pValue)==0?"1":"0"));
				*len=strlen(*value);
				return FOURD_OK;
			}
			case VK_BYTE:
			case VK_WORD:
			case VK_LONG:
			case VK_LONG8:
			case VK_DURATION:
			{
				*value=calloc(22,sizeof(char));
				sprintf_s(*value,22,"%d",*((FOURD_LONG *)elmt->pValue));
				*len=strlen(*value);
				return FOURD_OK;
			}
			case VK_REAL:
			{
				*value=calloc(64,sizeof(char));
				sprintf_s(*value,64,"%lf",*((FOURD_REAL *)elmt->pValue));
				*len=strlen(*value);
				return FOURD_OK;
			}
			case VK_FLOAT:
				return 0;
			case VK_TIME:
			case VK_TIMESTAMP:
			{
				FOURD_TIMESTAMP *t=elmt->pValue;
				unsigned int h,m,s,milli;
				milli=t->milli;
				h=milli/(60*60*1000);
				milli-=h*(60*60*1000);
				m=milli/(60*1000);
				milli-=m*(60*1000);
				s=milli/(1000);
				milli-=s*(1000);

				*value=calloc(24,sizeof(char));
				sprintf_s(*value,24,"%0.4d/%0.2d/%0.2d %0.2d:%0.2d:%0.2d.%0.3d",t->year,t->mounth,t->day,h,m,s,milli);
				*len=strlen(*value);
				return FOURD_OK;
			}
			case VK_STRING:
			{
				FOURD_STRING *str=elmt->pValue;
				int size=0;
				*value=NULL;
				size=str->length;
				*value=calloc(size,2);	/*2 bytes per char*/
				memcpy(*value,str->data,str->length*2);
				*len=str->length*2;
				return FOURD_OK;
			}
			case VK_BLOB:
			case VK_IMAGE:
				return FOURD_ERROR;
			default:
				return FOURD_ERROR;
		}
	}

	return 0;
}

/* Returns the col name */
const char * fourd_get_column_name(FOURD_RESULT *res,unsigned int numCol)
{
	unsigned int nbCol=res->row_type.nbColumn;

	if(numCol>=nbCol)
		return "";

	if(res->row_type.Column==NULL)
		return "";

	return res->row_type.Column[numCol].sColumnName;
}

/* Returns the col type */
FOURD_TYPE fourd_get_column_type(FOURD_RESULT *res,unsigned int numCol)
{
	unsigned int nbCol=res->row_type.nbColumn;
	FOURD_TYPE type=VK_UNKNOW;

	if(numCol>=nbCol)
		return 0;

	if(res->row_type.Column==NULL)
		return 0;

	type=res->row_type.Column[numCol].type;

	return type;
}

/* Returns numnber o columns */
int fourd_num_columns(FOURD_RESULT *res)
{
	return res->row_type.nbColumn;
}

/* Returns an allocated FOURD_STRING */
FOURD_STRING *fourd_create_string(char *param,int length){
	// Length is the character length of the string. Byte length is twice that due to UTF-16LE encoding
	FOURD_STRING *cp=NULL;

	cp=calloc(1,sizeof(FOURD_STRING));
	cp->data=calloc((size_t)length,2);	/* 2 bytes per char */
	cp->length=length;
	memcpy(cp->data,param,length*2);  /* 2 bytes per char */

	return cp;
}


int fourd_bind_param(FOURD_STATEMENT *state,unsigned int numParam,FOURD_TYPE type, void *val)
{
	/* realloc the size of memory if necessary */
	if(numParam>=state->nbAllocElement) {
		state->nbAllocElement=numParam+5; //FIXME
		state->elmt=realloc(state->elmt,(sizeof(FOURD_ELEMENT)*state->nbAllocElement));
	}

	if(numParam>=state->nb_element)
		state->nb_element=numParam+1;	/*zero-based index */


	state->elmt[numParam].type=type;

	if(val==NULL) {
		state->elmt[numParam].null=1;
		state->elmt[numParam].pValue=NULL;
	}
	else {
		state->elmt[numParam].null=0;
		state->elmt[numParam].pValue=_copy(type,val);
	}

	return 0;
}

void fourd_set_statement_preferred_image_types(FOURD_STATEMENT *state,const char *types)
{
	if(state->preferred_image_types)
		Free(state->preferred_image_types);

	if(types)	{
		state->preferred_image_types=malloc(strlen(types)+1);
		sprintf_s(state->preferred_image_types,strlen(types)+1,"%s",types);
	}
	else	{
		state->preferred_image_types=NULL;
	}
}

const char* fourd_get_preferred_image_types(FOURD* cnx)
{
	return cnx->preferred_image_types;
}

const char* fourd_get_statement_preferred_image_types(FOURD_STATEMENT *state)
{
	return state->preferred_image_types;
}


