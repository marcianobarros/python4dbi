#ifndef __UTILS__
#define __UTILS__ 1

#include <stdio.h> /* size_t */

/* Variable aArgument print function */
int Printf(const char* format,...);

/* Variable argument print error function */
int Printferr(const char* format,...);

/* String strip */
char *str_strip(char *s);

/* WIN32 functions */
#ifndef WIN32

/* ZeroMemory macro fills a block of memory with zeros */
void ZeroMemory (void *s, size_t n);

/* Breaks string str(a) into a series of tokens using the delimiter delim(b) */
#define strtok_s(a,b,c) strtok(a,b)

/*  Copies the null-terminated byte string pointed to by src, including the null terminator,
 * to the character array whose first element is pointed to by dest. */
#define strcpy_s(s,size,cs) strncpy(s,cs,size)
#define strncpy_s(s,ms,cs,size) strncpy(s,cs,size)

/* Write formatted data to a string */
int sprintf_s(char *buff,size_t size,const char* format,...);
int _snprintf_s(char *buff, size_t size, size_t count, const char *format,...);
int _snprintf(char *buff, int size, const char *format,...);

#endif /* WIN32 */


#endif /* __UTILS__ */
